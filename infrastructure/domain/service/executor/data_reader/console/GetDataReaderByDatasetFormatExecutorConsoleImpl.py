from application.domain.model.executor.data_reader.GetDataReaderByDatasetFormatExecutor import \
    GetDataReaderByDatasetFormatExecutor
from domain.model.dataset_format.DatasetFormat import FOLDER_OF_LABELS_CSV, ENCODING_128_CSV, ENCODING_128_CSV_1_OUTPUT, \
    FOLDER_OF_LABELS_CSV_BLACK
from infrastructure.domain.model.data_reader.encoding_128_csv.console.Encoding128CsvDataReaderImpl import \
    Encoding128CsvDataReaderImpl
from infrastructure.domain.model.data_reader.encoding_128_csv_1_output.console.Encoding128Csv1OutputDataReaderImpl import \
    Encoding128Csv1OutputDataReaderImpl
from infrastructure.domain.model.data_reader.folder_with_labels_csv.console.FolderWithLabelsCsvDataReaderImpl import \
    FolderWithLabelsCsvDataReaderImpl
from infrastructure.domain.model.data_reader.folder_with_labels_csv_black.console.FolderWithLabelsCsvBlackDataReaderImpl import \
    FolderWithLabelsCsvBlackDataReaderImpl


class GetDataReaderByDatasetFormatExecutorConsoleImpl(GetDataReaderByDatasetFormatExecutor):

    def execute(self, datasetFormat):
        datasetFormatName = datasetFormat.name
        if datasetFormatName == FOLDER_OF_LABELS_CSV:
            self.dataReader = FolderWithLabelsCsvDataReaderImpl()
        elif datasetFormatName == FOLDER_OF_LABELS_CSV_BLACK:
            self.dataReader = FolderWithLabelsCsvBlackDataReaderImpl()
        elif datasetFormatName == ENCODING_128_CSV:
            self.dataReader = Encoding128CsvDataReaderImpl()
        elif datasetFormatName == ENCODING_128_CSV_1_OUTPUT:
            self.dataReader = Encoding128Csv1OutputDataReaderImpl()
        else:
            input('The necessary dataReader could not be found by some reasons!')
            self.dataReader = None
