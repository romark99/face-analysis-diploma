from application.domain.service.executor.validator.ValidateDatasetAndArchitectureExecutor import \
    ValidateDatasetAndArchitectureExecutor


class ValidateDatasetAndArchitectureExecutorConsoleImpl(ValidateDatasetAndArchitectureExecutor):
    def execute(self, kerasModel, datasetFormat):
        print('Validation of inputs and outputs...')
        super().execute(kerasModel, datasetFormat)
        if not self.isValid:
            print('Error in validation of inputs and outputs!')
            return
        print('Validation of inputs and outputs finished.')
