import numpy as np

from application.domain.service.runner.predicting.PredictingFolderRunner import PredictingFolderRunner
from definitions import IS_USE_AGE_WITH_1_OUTPUT
from infrastructure.domain.model.executor.dataset.console.ChooseDatasetTypeExecutorConsoleImpl import \
    ChooseDatasetTypeExecutorConsoleImpl
from infrastructure.domain.model.executor.dataset.dialog.ChooseDataFolderExecutorDialogImpl import \
    ChooseDataFolderExecutorDialogImpl
from infrastructure.domain.model.executor.dataset_format.console.ChooseDatasetFormatNameExecutorConsoleImpl import \
    ChooseDatasetFormatNameExecutorConsoleImpl
from infrastructure.domain.model.runner.net_architecture.console.LoadNetArchitectureWithWeightsRunnerConsoleImpl import \
    LoadNetArchitectureWithWeightsRunnerConsoleImpl
from infrastructure.domain.model.utils.prediction.PredictionUtils import getSemanticPrediction
from infrastructure.domain.service.executor.data_reader.console.GetDataReaderByDatasetFormatExecutorConsoleImpl import \
    GetDataReaderByDatasetFormatExecutorConsoleImpl


class PredictingFolderRunnerConsoleImpl(PredictingFolderRunner):
    def __init__(self):
        super().__init__(LoadNetArchitectureWithWeightsRunnerConsoleImpl(),
                         GetDataReaderByDatasetFormatExecutorConsoleImpl())

    def readDatasetType(self):
        executor = ChooseDatasetTypeExecutorConsoleImpl()
        executor.execute()
        return executor.getDatasetType()

    def readInputFolder(self):
        executor = ChooseDataFolderExecutorDialogImpl()
        executor.execute()
        return executor.getDatasetFolderPath()

    def readDatasetFormatName(self):
        executor = ChooseDatasetFormatNameExecutorConsoleImpl()
        executor.execute()
        return executor.getDatasetFormatName()

    def displayPrediction(self, datasetType, prediction):
        if datasetType == 'age' and not IS_USE_AGE_WITH_1_OUTPUT:
            ages = np.arange(0, 116).reshape(116, 1)
            prediction = prediction.dot(ages).flatten()[0]
        semanticPrediction = getSemanticPrediction(datasetType, prediction)
        print(f'Predicted: {semanticPrediction}')
        print()

    def displayImageName(self, imageName):
        print(f'{imageName}')

    def displayImageNotDetected(self):
        print(f'Face was not detected.')
        print()

    def displayFolderIsNotChosen(self):
        print(f'The folder was not chosen.')
        print()
