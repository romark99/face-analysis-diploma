from application.domain.service.runner.recognition.TraningRunner import TrainingRunner
from infrastructure.domain.model.executor.dataset.console.ChooseDatasetTypeExecutorConsoleImpl import \
    ChooseDatasetTypeExecutorConsoleImpl
from infrastructure.domain.model.executor.keras_model.TrainKerasModelExecutorConsoleImpl import \
    TrainKerasModelExecutorConsoleImpl
from infrastructure.domain.model.executor.training_results.console.ProcessAgeTrainingResultsExecutorConsoleImpl import \
    ProcessAgeTrainingResultsExecutorConsoleImpl
from infrastructure.domain.model.reader.net_params.console.NetParamsReaderConsoleImpl import NetParamsReaderConsoleImpl
from infrastructure.domain.model.repository.net_architecture.json.NetArchitectureRepositoryJSONImpl import \
    NetArchitectureRepositoryJSONImpl
from infrastructure.domain.model.runner.dataset.console.DetermineDatasetRunnerConsoleImpl import \
    DetermineDatasetRunnerConsoleImpl

from infrastructure.domain.model.runner.net_architecture.console.DetermineNetArchitectureRunnerConsoleImpl import \
    DetermineNetArchitectureRunnerConsoleImpl
from infrastructure.domain.model.runner.net_architecture.console.LoadNetArchitectureWithWeightsRunnerConsoleImpl import \
    LoadNetArchitectureWithWeightsRunnerConsoleImpl
from infrastructure.domain.model.runner.training_results.console.ProcessTrainingResultsRunnerConsoleImpl import \
    ProcessTrainingResultsRunnerConsoleImpl
from infrastructure.domain.model.utils.reader.console import ConsoleReaderUtils
from infrastructure.domain.service.executor.data_reader.console.GetDataReaderByDatasetFormatExecutorConsoleImpl import \
    GetDataReaderByDatasetFormatExecutorConsoleImpl
from infrastructure.domain.service.executor.validator.console.ValidateDatasetAndArchitectureExecutorConsoleImpl import \
    ValidateDatasetAndArchitectureExecutorConsoleImpl
from infrastructure.domain.service.runner.recognition.console.FaceRecognitionRunnerConsoleImpl import \
    FaceRecognitionRunnerConsoleImpl


class TrainingRunnerConsoleImpl(TrainingRunner):
    def isToUseSavedModel(self):
        result = ConsoleReaderUtils.readConsoleYesNo("Do you want to choose an existing network model?")
        return result

    def processResults(self, datasetType, datasetFormatName, kerasHistory, saveFolderPath, totalTime, imageNames):
        if datasetType == 'age':
            processAgeTrainingResultsExecutor = ProcessAgeTrainingResultsExecutorConsoleImpl()
            processAgeTrainingResultsExecutor.execute(datasetFormatName, kerasHistory, saveFolderPath, totalTime,
                                                      imageNames)
        else:
            processResultsRunner = ProcessTrainingResultsRunnerConsoleImpl()
            processResultsRunner.setKerasHistory(kerasHistory)
            processResultsRunner.setResultsFolderPath(saveFolderPath)
            processResultsRunner.setTotalTime(totalTime)
            processResultsRunner.setImageNames(imageNames)
            processResultsRunner.run()

    def isUseFaceRecognitionModule(self):
        result = ConsoleReaderUtils.readConsoleYesNo('Do you want to use "face-recognition" module?')
        return result

    def runFaceRecognition(self, datasetType):
        runner = FaceRecognitionRunnerConsoleImpl()
        runner.run(datasetType)

    def __init__(self):
        repo = NetArchitectureRepositoryJSONImpl()
        super().__init__(
            ChooseDatasetTypeExecutorConsoleImpl(),
            DetermineNetArchitectureRunnerConsoleImpl(repo),
            LoadNetArchitectureWithWeightsRunnerConsoleImpl(),
            NetParamsReaderConsoleImpl(),
            DetermineDatasetRunnerConsoleImpl(),
            GetDataReaderByDatasetFormatExecutorConsoleImpl(),
            ValidateDatasetAndArchitectureExecutorConsoleImpl(),
            TrainKerasModelExecutorConsoleImpl(),
        )
