import csv
import os
import random

import numpy as np
from sklearn.model_selection import train_test_split

from application.domain.service.runner.recognition.FaceRecognitionRunner import FaceRecognitionRunner
from definitions import DIR_DATA_FULL_PATH, DIR_DATASET_NAME, ENTER_ANY_STRING_TO_CONTINUE, \
    DATASET_TYPE_FOLDER_IS_NOT_FOUND, DATASETS_OF_FOLLOWING_TYPE_NOT_FOUND, WRONG_OPTION_MESSAGE
from domain.model.dataset_format import DatasetFormat
from infrastructure.domain.model.executor.dataset.console.ChooseDatasetNameByTypeExecutorConsoleImpl import \
    ChooseDatasetNameByTypeExecutorConsoleImpl
from infrastructure.domain.model.runner.dataset_format.console.DetermineDatasetFormatRunnerConsoleImpl import \
    DetermineDatasetFormatRunnerConsoleImpl


class FaceRecognitionRunnerConsoleImpl(FaceRecognitionRunner):

    def readDatasetName(self, datasetType):
        chooseDatasetNameByTypeExecutor = ChooseDatasetNameByTypeExecutorConsoleImpl()
        chooseDatasetNameByTypeExecutor.execute(datasetType)
        datasetName = chooseDatasetNameByTypeExecutor.getDatasetName()
        return datasetName

    def determineDatasetFormat(self, dataset, datasetFormatName):
        determineDatasetFormatRunner = DetermineDatasetFormatRunnerConsoleImpl()
        determineDatasetFormatRunner.setDataset(dataset)
        determineDatasetFormatRunner.run(datasetFormatName)
        dataset = determineDatasetFormatRunner.getDataset()
        return dataset

    def run(self, datasetType):
        print("Running face recognition module...")
        # try:
        super().run(datasetType)
        # except TypeError as e:
        #     print(f"TypeError: {e}")
        #     return
        print("Running face recognition module finished.")

    def chooseUnknownDataset(self, datasetType):
        self.datasetPath = None
        while True:
            print("Choose unknown dataset: ")
            i = 0
            datasetTypePath = os.path.join(DIR_DATA_FULL_PATH, datasetType, DIR_DATASET_NAME)
            if not os.path.exists(datasetTypePath):
                print(DATASET_TYPE_FOLDER_IS_NOT_FOUND)
                input(ENTER_ANY_STRING_TO_CONTINUE)
                return None
            files = os.listdir(datasetTypePath)
            dirs = list(filter(lambda file: os.path.isdir(os.path.join(datasetTypePath, file)), files))
            if not dirs:
                self.datasetName = None
                print(DATASETS_OF_FOLLOWING_TYPE_NOT_FOUND)
                input(ENTER_ANY_STRING_TO_CONTINUE)
                return None
            dirs.sort()
            datasetNamesDict = dict()
            for typeDirName in dirs:
                i += 1
                print(f"'{i}' : {typeDirName}")
                datasetNamesDict[str(i)] = typeDirName
            choice = input()
            if choice in datasetNamesDict.keys():
                return datasetNamesDict[choice]
            print(WRONG_OPTION_MESSAGE)

    def getFaceData(self, dataset, unknownDatasetName):
        unknownDatasetPath = os.path.join(DIR_DATA_FULL_PATH, dataset.type, DIR_DATASET_NAME, unknownDatasetName,
                                          dataset.format.name, f'{DatasetFormat.ENCODING_128_CSV}.csv')

        csvPath = os.path.join(dataset.path, f'{DatasetFormat.ENCODING_128_CSV}.csv')

        def read_encodings():
            encodings = []
            encodings_label = []
            img_names = []
            with open(csvPath) as f:
                csv_reader = csv.reader(f, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    img_name = row.pop(0)
                    label = row.pop(0)
                    encodings.append(row)
                    encodings_label.append(label)
                    img_names.append(img_name)
                    line_count += 1
                print(f'ALL: Processed {line_count} lines.')
            return (encodings, encodings_label, img_names)

        X, Y, IMG = read_encodings()

        x_train, x_test, y_train, y_test, img_names_train, img_names_test = \
            train_test_split(X, Y, IMG, test_size=0.66)

        with open(unknownDatasetPath) as f:
            csv_reader = csv.reader(f, delimiter=',')
            line_count = 0
            csv_array = []
            for row in csv_reader:
                csv_array.append(row)
            random.shuffle(csv_array)

            sizeOfRandomDataset = (len(x_test) // 4) if (len(x_test) // 4 < len(csv_array)) else (len(csv_array))
            csv_array = csv_array[:sizeOfRandomDataset]

            for row in csv_array:
                img_name = row.pop(0)
                label = row.pop(0)
                x_test.append(row)
                y_test.append('unknown')
                img_names_test.append(img_name)
                line_count += 1
        print(f'UNKNOWN: Processed {line_count} lines.')

        x_train = np.array(x_train, 'float32')
        x_test = np.array(x_test, 'float32')
        return x_train, x_test, y_train, y_test, img_names_train, img_names_test
