import csv
import os

import numpy as np
from sklearn.model_selection import train_test_split

from application.domain.model.data_reader.DataReader import DataReader
from domain.model.dataset_format import DatasetFormat
from infrastructure.domain.model.utils.reformatter import ReformatterUtils


class Encoding128Csv1OutputDataReaderImpl(DataReader):
    def __init__(self):
        super().__init__()

    def read(self, datasetFormatPath, inputShape, numberOfLabels):
        csvPath = os.path.join(datasetFormatPath, f'{DatasetFormat.ENCODING_128_CSV}.csv')

        ageLabels = 116

        def read_encodings():
            encodings = []
            encodings_label = []
            img_names = []
            with open(csvPath) as f:
                csv_reader = csv.reader(f, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    img_name = row.pop(0)
                    label = int(row.pop(0))
                    encodings.append(row)
                    encodings_label.append(label)
                    img_names.append(img_name)
                    line_count += 1
                print(f'ALL: Processed {line_count} lines.')

            return (encodings, encodings_label, img_names)

        X, Y, IMG = read_encodings()

        x_train, x_test, y_train, y_test, img_names_train, img_names_test = \
            train_test_split(X, Y, IMG, test_size=0.1)
        print(f'Training: Processed {len(x_train)} lines.')
        print(f'Testing: Processed {len(x_test)} lines.')

        x_train = np.array(x_train, 'float32')
        y_train = np.array(y_train, 'float32')
        x_test = np.array(x_test, 'float32')
        y_test = np.array(y_test, 'float32')

        if min(y_train) > 0:
            y_train = y_train - min(y_train)
        if min(y_test) > 0:
            y_test = y_test - min(y_test)
        y_train = (y_train / ageLabels).reshape(-1, 1)
        y_test = (y_test / ageLabels).reshape(-1, 1)

        data = x_train, x_test, y_train, y_test
        return data, (img_names_train, img_names_test)

    def readFromWildImagePath(self, imagePath, inputShape):
        x_test = ReformatterUtils.fromWildToEncoding128CsvArray(imagePath, inputShape)
        if x_test is None:
            return None
        x_test = np.array(x_test, 'float32')
        x_test -= np.mean(x_test, axis=0)
        x_test /= np.std(x_test, axis=0)
        x_test = x_test.reshape(1, *inputShape)
        return x_test
