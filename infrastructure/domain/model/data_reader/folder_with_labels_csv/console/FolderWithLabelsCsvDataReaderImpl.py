import os

import numpy as np
import pandas as pd
from keras.utils import np_utils
from sklearn.model_selection import train_test_split

from application.domain.model.data_reader.DataReader import DataReader
from domain.model.dataset_format import DatasetFormat


class FolderWithLabelsCsvDataReaderImpl(DataReader):
    def __init__(self):
        super().__init__()

    def read(self, datasetFormatPath, inputShape, numberOfLabels):

        csvPath = os.path.join(datasetFormatPath, f'{DatasetFormat.FOLDER_OF_LABELS_CSV}.csv')

        print(f'DatasetPath is : {csvPath}')
        df = pd.read_csv(csvPath)

        X, Y = [], []

        for index, row in df.iterrows():
            val = row['pixels'].split(" ")
            try:
                X.append(np.array(val, 'float32'))
                Y.append(row['label'])
            except:
                print(f"Error occured at index :{index} and row:{row}")

        x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=4)

        x_train = np.array(x_train, 'float32')
        y_train = np.array(y_train, 'float32')
        x_test = np.array(x_test, 'float32')
        y_test = np.array(y_test, 'float32')

        y_train = np_utils.to_categorical(y_train, num_classes=numberOfLabels)
        y_test = np_utils.to_categorical(y_test, num_classes=numberOfLabels)

        # cannot produce
        # normalizing data between oand 1
        x_train -= np.mean(x_train, axis=0)
        x_train /= np.std(x_train, axis=0)

        x_test -= np.mean(x_test, axis=0)
        x_test /= np.std(x_test, axis=0)

        # X_train = X_train.reshape(X_train.shape[0], 48, 48, 1)
        x_train = x_train.reshape(x_train.shape[0], *inputShape)

        # X_test = X_test.reshape(X_test.shape[0], 48, 48, 1)
        x_test = x_test.reshape(x_test.shape[0], *inputShape)

        data = x_train, x_test, y_train, y_test
        return data, None
