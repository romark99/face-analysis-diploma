from definitions import DATASET_TYPES
from domain.model.net_architecture.NetArchitecture import NetArchitecture
from domain.model.net_architecture.reader.NetArchitectureReader import NetArchitectureReader
from infrastructure.domain.model.reader.net_layer.console.NetLayerReaderConsoleImpl import NetLayerReaderConsoleImpl
from infrastructure.domain.model.reader.net_layer.input.console.InputNetLayerReaderConsoleImpl import \
    InputNetLayerReaderConsoleImpl
from infrastructure.domain.model.utils.reader.console import ConsoleReaderUtils


class NetArchitectureReaderConsoleImpl(NetArchitectureReader):

    def read(self, datasetType=None):
        print("Net architecture creation...")
        entity = super().read(datasetType)
        print("Net architecture creation finished.")
        return entity

    def readName(self):
        while True:
            value = input('Enter the architecture name: ')
            newValue, errorMessage = NetArchitecture.validateName(value)
            if errorMessage is None:
                return value
            print(errorMessage)

    def readInputLayer(self):
        inputLayerReader = InputNetLayerReaderConsoleImpl()
        inputLayer = inputLayerReader.read()
        return inputLayer

    def readLayer(self, isWithInput):
        netLayerReader = NetLayerReaderConsoleImpl()
        layer = netLayerReader.read(isWithInput=isWithInput)
        return layer

    def readType(self):
        datasetType = ConsoleReaderUtils.chooseConsoleVariant(DATASET_TYPES, 'Choose dataset type: ')
        return datasetType

    def displayNetArchitectureSummary(self, summary):
        print(summary)
