from definitions import LOSSES, MONITORS, OPTIMIZERS, DEFAULT_ALPHAS, DEFAULT_MONITOR, DEFAULT_LOSS, DEFAULT_OPTIMIZER
from domain.model.net_params.NetParams import NetParams
from domain.model.net_params.reader.NetParamsReader import NetParamsReader
from infrastructure.domain.model.utils.reader.console import ConsoleReaderUtils


class NetParamsReaderConsoleImpl(NetParamsReader):
    def readBatchSize(self):
        while True:
            value = input('Enter batch size (positive integer, default value: 32): ')
            newValue, errorMessage = NetParams.validateBatchSize(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)

    def readEpochs(self):
        while True:
            value = input('Enter max number of epochs (positive integer, default value: 100): ')
            newValue, errorMessage = NetParams.validateEpochs(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)

    def readLoss(self):
        value = ConsoleReaderUtils.chooseConsoleVariant(LOSSES,
                                                        'Choose a loss function (default: mean_squared_error): ',
                                                        DEFAULT_LOSS)
        return value

    def isToUseEarlyStopping(self):
        isToUseEarlyStopping = ConsoleReaderUtils.readConsoleYesNo('Do you want to use Early Stopping?')
        return isToUseEarlyStopping

    def readEarlyStoppingMonitor(self):
        value = ConsoleReaderUtils.chooseConsoleVariant(MONITORS,
                                                        'Choose what you want to monitor in Early Stopping (default: val_loss): ',
                                                        DEFAULT_MONITOR)
        return value

    def readOptimizerName(self):
        value = ConsoleReaderUtils.chooseConsoleVariant(OPTIMIZERS,
                                                        'Choose optimizer (default: SGD): ',
                                                        DEFAULT_OPTIMIZER)
        return value

    def readEarlyStoppingPatience(self):
        while True:
            value = input('Enter patience for Early Stopping (positive integer, default value: 10): ')
            newValue, errorMessage = NetParams.validateEarlyStoppingPatience(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)

    def readOptimizerAlpha(self, optimizerName):
        defaultAlpha = DEFAULT_ALPHAS[optimizerName]
        while True:
            value = input(f'Enter learning rate for Optimizer (positive float, default value: {defaultAlpha}): ')
            newValue, errorMessage = NetParams.validateOptimizerAlpha(value, defaultAlpha)
            if errorMessage is None:
                return newValue
            print(errorMessage)

    def read(self):
        print("Reading net params...")
        netParams = super().read()
        print("Reading net params finished.")
        return netParams
