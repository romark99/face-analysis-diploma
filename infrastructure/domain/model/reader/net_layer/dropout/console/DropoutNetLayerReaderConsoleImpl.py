from domain.model.net_layer.dropout.DropoutNetLayer import DropoutNetLayer
from domain.model.net_layer.dropout.reader.DropoutNetLayerReader import DropoutNetLayerReader


class DropoutNetLayerReaderConsoleImpl(DropoutNetLayerReader):
    def read(self):
        print("Dropout layer creation...")
        entity = super().read()
        print("Dropout layer creation finished.")
        return entity

    def readRate(self):
        while True:
            value = input('Enter a rate (a float between 0 and 1): ')
            newValue, errorMessage = DropoutNetLayer.validateRate(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)
