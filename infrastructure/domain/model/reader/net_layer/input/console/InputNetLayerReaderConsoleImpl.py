from domain.model.net_layer.input.InputNetLayer import InputNetLayer
from domain.model.net_layer.input.reader.InputNetLayerReader import InputNetLayerReader


class InputNetLayerReaderConsoleImpl(InputNetLayerReader):
    def read(self):
        print("Input layer creation...")
        entity = super().read()
        print("Input layer creation finished.")
        return entity

    def readInputShape(self):
        while True:
            value = input('Enter an input shape (tuple of 1 to 3 integers): ')
            newValue, errorMessage = InputNetLayer.validateInputShape(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)
