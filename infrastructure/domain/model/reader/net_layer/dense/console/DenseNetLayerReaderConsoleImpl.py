from definitions import ACTIVATIONS, DEFAULT_ACTIVATION
from domain.model.net_layer.dense.reader.DenseNetLayerReader import DenseNetLayerReader
from infrastructure.domain.model.utils.reader.console.ConsoleReaderUtils import chooseConsoleVariant


class DenseNetLayerReaderConsoleImpl(DenseNetLayerReader):
    def read(self):
        print("Dense layer creation...")
        entity = super().read()
        print("Dense layer creation finished.")
        return entity

    def readUnits(self):
        while True:
            value = input('Enter number of units (positive integer): ')
            newValue, errorMessage = self.entity.validateUnits(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)

    def readActivation(self):
        value = chooseConsoleVariant(ACTIVATIONS, 'Choose activation (default: linear): ', DEFAULT_ACTIVATION)
        return value
