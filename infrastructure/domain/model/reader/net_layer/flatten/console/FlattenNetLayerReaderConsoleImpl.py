from domain.model.net_layer.flatten.reader.FlattenNetLayerReader import FlattenNetLayerReader


class FlattenNetLayerReaderConsoleImpl(FlattenNetLayerReader):
    def read(self):
        print("Flatten layer creation...")
        entity = super().read()
        print("Flatten layer creation finished.")
        return entity
