from definitions import PADDINGS, DEFAULT_PADDING
from domain.model.net_layer.pooling.PoolingNetLayer import PoolingNetLayer
from domain.model.net_layer.pooling.reader.PoolingNetLayerReader import PoolingNetLayerReader
from infrastructure.domain.model.utils.reader.console.ConsoleReaderUtils import chooseConsoleVariant


class PoolingNetLayerReaderConsoleImpl(PoolingNetLayerReader):
    def read(self):
        print("Pooling layer creation...")
        entity = super().read()
        print("Pooling layer creation finished.")
        return entity

    def readPoolSize(self):
        while True:
            value = input('Enter a pool size (an integer or tuple of 2 integers): ')
            newValue, errorMessage = PoolingNetLayer.validatePoolSize(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)

    def readPadding(self):
        value = chooseConsoleVariant(PADDINGS, 'Choose type of padding (default: valid): ', DEFAULT_PADDING)
        return value
