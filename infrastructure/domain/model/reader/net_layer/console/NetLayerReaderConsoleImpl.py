from definitions import CHOOSE_NET_LAYER_TYPE
from domain.model.net_layer.NetLayer import NetLayer
from domain.model.net_layer.reader.NetLayerReader import NetLayerReader
from infrastructure.domain.model.reader.net_layer.activation.console.ActivationNetLayerReaderConsoleImpl import \
    ActivationNetLayerReaderConsoleImpl
from infrastructure.domain.model.reader.net_layer.batch_normalization.console.BatchNormalizationNetLayerReaderConsoleImpl import \
    BatchNormalizationNetLayerReaderConsoleImpl
from infrastructure.domain.model.reader.net_layer.convolutional.console.ConvolutionalNetLayerReaderConsoleImpl import \
    ConvolutionalNetLayerReaderConsoleImpl
from infrastructure.domain.model.reader.net_layer.dense.console.DenseNetLayerReaderConsoleImpl import \
    DenseNetLayerReaderConsoleImpl
from infrastructure.domain.model.reader.net_layer.dropout.console.DropoutNetLayerReaderConsoleImpl import \
    DropoutNetLayerReaderConsoleImpl
from infrastructure.domain.model.reader.net_layer.flatten.console.FlattenNetLayerReaderConsoleImpl import \
    FlattenNetLayerReaderConsoleImpl
from infrastructure.domain.model.reader.net_layer.pooling.console.PoolingNetLayerReaderConsoleImpl import \
    PoolingNetLayerReaderConsoleImpl
from infrastructure.domain.model.utils.reader.console.ConsoleReaderUtils import chooseConsoleVariant


class NetLayerReaderConsoleImpl(NetLayerReader):

    def read(self, isWithInput) -> None:
        netLayerType = self.chooseNetLayerType(isWithInput)
        netLayerReader = self.getLayerReaderByType(netLayerType)
        if netLayerReader is None:
            return None
        return netLayerReader.read()

    def getLayerReaderByType(self, netLayerType):
        netLayerReader = None
        if netLayerType == NetLayer.CONVOLUTIONAL:
            netLayerReader = ConvolutionalNetLayerReaderConsoleImpl()
        elif netLayerType == NetLayer.DENSE:
            netLayerReader = DenseNetLayerReaderConsoleImpl()
        elif netLayerType == NetLayer.FLATTEN:
            netLayerReader = FlattenNetLayerReaderConsoleImpl()
        elif netLayerType == NetLayer.ACTIVATION:
            netLayerReader = ActivationNetLayerReaderConsoleImpl()
        elif netLayerType == NetLayer.BATCH_NORMALIZATION:
            netLayerReader = BatchNormalizationNetLayerReaderConsoleImpl()
        elif netLayerType == NetLayer.DROPOUT:
            netLayerReader = DropoutNetLayerReaderConsoleImpl()
        elif netLayerType == NetLayer.POOLING:
            netLayerReader = PoolingNetLayerReaderConsoleImpl()
        return netLayerReader

    def chooseNetLayerType(self, isWithInput):
        LITERAL_FINISH_CREATING_LAYERS = "Finish creating layers"

        adderDict = None
        if not isWithInput:
            adderDict = {'*': LITERAL_FINISH_CREATING_LAYERS}
        value = chooseConsoleVariant(NetLayer.TYPES, CHOOSE_NET_LAYER_TYPE, adderDict=adderDict)
        if value == LITERAL_FINISH_CREATING_LAYERS:
            return None
        return value
