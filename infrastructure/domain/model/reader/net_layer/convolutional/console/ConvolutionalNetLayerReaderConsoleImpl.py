from definitions import ACTIVATIONS, PADDINGS, DEFAULT_ACTIVATION, DEFAULT_PADDING
from domain.model.net_layer.convolutional.ConvolutionalNetLayer import ConvolutionalNetLayer
from domain.model.net_layer.convolutional.reader.ConvolutionalNetLayerReader import ConvolutionalNetLayerReader
from infrastructure.domain.model.utils.reader.console.ConsoleReaderUtils import chooseConsoleVariant


class ConvolutionalNetLayerReaderConsoleImpl(ConvolutionalNetLayerReader):
    def read(self):
        print("Convolutional layer creation...")
        entity = super().read()
        print("Convolutional layer creation finished.")
        return entity

    def readFilters(self):
        while True:
            value = input('Enter number of filters (positive integer): ')
            newValue, errorMessage = ConvolutionalNetLayer.validateFilters(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)

    def readKernelSize(self):
        while True:
            value = input('Enter a kernel size (an integer or tuple of 2 integers): ')
            newValue, errorMessage = ConvolutionalNetLayer.validateKernelSize(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)

    def readActivation(self):
        value = chooseConsoleVariant(ACTIVATIONS, 'Choose activation (default: linear): ', DEFAULT_ACTIVATION)
        return value

    def readPadding(self):
        value = chooseConsoleVariant(PADDINGS, 'Choose type of padding (default: valid): ', DEFAULT_PADDING)
        return value
