from definitions import ACTIVATIONS, DEFAULT_ACTIVATION
from domain.model.net_layer.activation.reader.ActivationNetLayerReader import ActivationNetLayerReader
from infrastructure.domain.model.utils.reader.console.ConsoleReaderUtils import chooseConsoleVariant


class ActivationNetLayerReaderConsoleImpl(ActivationNetLayerReader):
    def read(self):
        print("Activation layer creation...")
        entity = super().read()
        print("Activation layer creation finished.")
        return entity

    def readActivation(self):
        value = chooseConsoleVariant(ACTIVATIONS, 'Choose activation (default: linear): ', DEFAULT_ACTIVATION)
        return value
