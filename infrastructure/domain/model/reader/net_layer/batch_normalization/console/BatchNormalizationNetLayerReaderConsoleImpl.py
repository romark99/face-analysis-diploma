from domain.model.net_layer.batch_normalization.BatchNormalizationNetLayer import BatchNormalizationNetLayer
from domain.model.net_layer.batch_normalization.reader.BatchNormalizationNetLayerReader import \
    BatchNormalizationNetLayerReader


class BatchNormalizationNetLayerReaderConsoleImpl(BatchNormalizationNetLayerReader):
    def read(self):
        print("BatchNormalization layer creation...")
        entity = super().read()
        print("BatchNormalization layer creation finished.")
        return entity

    def readAxis(self):
        while True:
            value = input('Enter the axis that should be normalized (integer from -1 to 2, default value: -1): ')
            newValue, errorMessage = BatchNormalizationNetLayer.validateAxis(value)
            if errorMessage is None:
                return newValue
            print(errorMessage)
