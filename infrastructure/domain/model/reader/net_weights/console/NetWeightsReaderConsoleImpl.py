from domain.model.net_weights.reader.NetWeightsReader import NetWeightsReader
from infrastructure.domain.model.utils.reader.console import ConsoleReaderUtils


class NetWeightsReaderConsoleImpl(NetWeightsReader):
    def readNetWeightsName(self, netWeightsNames):
        name = ConsoleReaderUtils.chooseConsoleVariant(netWeightsNames, 'Choose model folder: ')
        return name
