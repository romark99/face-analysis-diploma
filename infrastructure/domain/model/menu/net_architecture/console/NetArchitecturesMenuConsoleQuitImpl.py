from application.domain.model.menu.net_architecture.NetArchitecturesMenu import NetArchitecturesMenu
from domain.model.menu.menu_choice.MenuChoice import MenuChoice
from infrastructure.domain.model.menu.console.ConsoleQuitMenu import ConsoleQuitMenu
from infrastructure.domain.model.repository.net_architecture.json.NetArchitectureRepositoryJSONImpl import \
    NetArchitectureRepositoryJSONImpl
from infrastructure.domain.model.runner.net_architecture.console.BuildNetArchitectureRunnerConsoleImpl import \
    BuildNetArchitectureRunnerConsoleImpl
from infrastructure.domain.model.runner.net_architecture.console.DisplayNetArchitecturesListRunnerConsoleImpl import \
    DisplayNetArchitecturesListRunnerConsoleImpl


class NetArchitecturesMenuConsoleQuitImpl(NetArchitecturesMenu, ConsoleQuitMenu):
    def __init__(self):
        title = '''
******************************************
*         WORK WITH ARCHITECTURES        *
******************************************'''
        netArchitectureRepo = NetArchitectureRepositoryJSONImpl()
        choices = {
            '1': MenuChoice(self.LIST_NET_ARCHITECTURES,
                            DisplayNetArchitecturesListRunnerConsoleImpl(netArchitectureRepo)),
            '2': MenuChoice(self.CREATE_NET_ARCHITECTURE, BuildNetArchitectureRunnerConsoleImpl(netArchitectureRepo)),
        }

        super().__init__(title, choices)
