from application.domain.model.menu.dataset.DatasetsMenu import DatasetsMenu
from domain.model.menu.menu_choice.MenuChoice import MenuChoice
from infrastructure.domain.model.menu.console.ConsoleQuitMenu import ConsoleQuitMenu
from infrastructure.domain.model.runner.dataset.console.BuildDatasetRunnerConsoleImpl import \
    BuildDatasetRunnerConsoleImpl
from infrastructure.domain.model.runner.dataset.console.ChooseDatasetRunnerConsoleImpl import \
    ChooseDatasetRunnerConsoleImpl
from infrastructure.domain.model.runner.dataset.console.DisplayDatasetsListRunnerConsoleImpl import \
    DisplayDatasetsListRunnerConsoleImpl


class DatasetsMenuConsoleQuitImpl(DatasetsMenu, ConsoleQuitMenu):
    def __init__(self):
        title = '''
******************************************
*           WORK WITH DATASETS           *
******************************************'''

        choices = {
            '1': MenuChoice(self.LIST_DATASETS, DisplayDatasetsListRunnerConsoleImpl()),
            '2': MenuChoice(self.ADD_DATASET, BuildDatasetRunnerConsoleImpl()),
            '3': MenuChoice(self.REFORMAT_DATASET, ChooseDatasetRunnerConsoleImpl()),
        }

        super().__init__(title, choices)
