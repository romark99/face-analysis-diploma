from definitions import WRONG_OPTION_MESSAGE
from domain.model.menu.Menu import Menu


class ConsoleMenu(Menu):

    def displayChoices(self):
        for key, value in self.choices.items():
            print(f"'{key}' : {value.label}")

    def run(self):
        while True:
            # try:
            print()
            print(self.title)
            print(self.CHOOSE_OPTION)
            self.displayChoices()
            choice = input()
            if choice in self.choices.keys():
                self.choices[choice].runner.run()
            else:
                print(WRONG_OPTION_MESSAGE)
        # except AttributeError:
        #     print(FEATURE_IS_NOT_IMPLEMENTED_YET)
        #     input(ENTER_ANY_STRING_TO_CONTINUE)
