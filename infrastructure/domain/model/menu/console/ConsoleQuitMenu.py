from definitions import WRONG_OPTION_MESSAGE, ENTER_ANY_STRING_TO_CONTINUE, \
    SOMETHING_GOT_WRONG, IGNORE_ERRORS
from domain.model.menu.QuitMenu import QuitMenu

class ConsoleQuitMenu(QuitMenu):

    def displayChoices(self):
        for key, value in self.choices.items():
            print(f"'{key}' : {value.label}")
        print(f"'{self.BACK_CHAR}' : {self.BACK_LABEL}")
        print(f"'{self.CLOSE_CHAR}' : {self.CLOSE_LABEL}")

    def run(self):
        while True:
            try:
                choice = self.runQuitMenu()
                if choice == self.CLOSE_CHAR:
                    exit(0)
                elif choice == self.BACK_CHAR:
                    return
            except Exception as e:
                if IGNORE_ERRORS:
                    print(SOMETHING_GOT_WRONG)
                    print(e)
                    input(ENTER_ANY_STRING_TO_CONTINUE)
                else:
                    raise e

    def runQuitMenu(self):
        while True:
            print()
            print(self.title)
            print(self.CHOOSE_OPTION)
            self.displayChoices()
            choice = input()
            if choice == self.CLOSE_CHAR or choice == self.BACK_CHAR:
                return choice
            elif choice in self.choices.keys():
                self.choices[choice].runner.run()
            else:
                print(WRONG_OPTION_MESSAGE)
