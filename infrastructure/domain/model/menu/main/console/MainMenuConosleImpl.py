from application.domain.model.menu.main.MainMenu import MainMenu
from domain.model.menu.menu_choice.MenuChoice import MenuChoice
from infrastructure.domain.model.menu.console.ConsoleQuitMenu import ConsoleQuitMenu
from infrastructure.domain.model.menu.dataset.console.DatasetsMenuConsoleImpl import DatasetsMenuConsoleQuitImpl
from infrastructure.domain.model.menu.net_architecture.console.NetArchitecturesMenuConsoleQuitImpl import \
    NetArchitecturesMenuConsoleQuitImpl
from infrastructure.domain.model.runner.program_info.console.DisplayProgramInfoRunnerConsoleImpl import \
    DisplayProgramInfoRunnerConsoleImpl
from infrastructure.domain.service.runner.predicting.console.PredictingFolderRunnerConsoleImpl import \
    PredictingFolderRunnerConsoleImpl
from infrastructure.domain.service.runner.recognition.console.TrainingRunnerConsoleImpl import TrainingRunnerConsoleImpl


class MainMenuConsoleQuitImpl(MainMenu, ConsoleQuitMenu):

    def __init__(self):
        title = '''
******************************************
*          FACE ANALYSIS SYSTEM          *
******************************************'''

        choices = {
            '1': MenuChoice(self.TRAINING, TrainingRunnerConsoleImpl()),
            '2': MenuChoice(self.PREDICTING_A_FOLDER_WITH_IMAGES, PredictingFolderRunnerConsoleImpl()),
            # '3': MenuChoice(self.WEBCAM_RECOGNITION, None),
            '3': MenuChoice(self.DATASETS, DatasetsMenuConsoleQuitImpl()),
            '4': MenuChoice(self.NETWORK_ARCHITECTURES, NetArchitecturesMenuConsoleQuitImpl()),
            '5': MenuChoice(self.PROGRAM_INFO, DisplayProgramInfoRunnerConsoleImpl())
        }

        MainMenu.__init__(self, title, choices)
