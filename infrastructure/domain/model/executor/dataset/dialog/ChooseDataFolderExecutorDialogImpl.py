import tkinter as tk
from tkinter import filedialog

from application.domain.model.executor.dataset.ChooseDataFolderExecutor import ChooseDataFolderExecutor
from definitions import CHOOSE_DATASET_FOLDER, DATASET_FOLDER_IS_NOT_CHOSEN


class ChooseDataFolderExecutorDialogImpl(ChooseDataFolderExecutor):

    def execute(self):
        print(CHOOSE_DATASET_FOLDER)

        root = tk.Tk()
        root.withdraw()

        file_path = filedialog.askdirectory()
        if file_path == ():
            input(DATASET_FOLDER_IS_NOT_CHOSEN)
            file_path = None
        else:
            print(file_path)
        self.datasetFolderPath = file_path
