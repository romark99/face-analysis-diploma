import os

from application.domain.model.executor.dataset.DisplayDatasetsListExecutor import DisplayDatasetsListExecutor
from definitions import ENTER_ANY_STRING_TO_CONTINUE, STAR_STRING, DIR_DATA_FULL_PATH, \
    DIR_DATASET_NAME


class DisplayDatasetsListExecutorConsoleImpl(DisplayDatasetsListExecutor):

    def execute(self):
        print(STAR_STRING)
        dirs = os.listdir(DIR_DATA_FULL_PATH)
        dirs = [file for file in dirs if os.path.isdir(os.path.join(DIR_DATA_FULL_PATH, file))]
        dirs.sort()
        for typeDirName in dirs:
            print(f'{typeDirName} datasets:')
            typeDirFullPath = os.path.join(DIR_DATA_FULL_PATH, typeDirName, DIR_DATASET_NAME)
            datasetNames = os.listdir(typeDirFullPath)
            datasetNames.sort()
            for datasetName in datasetNames:
                print(f'----- {datasetName}')
            print()
        print(STAR_STRING)
        input(ENTER_ANY_STRING_TO_CONTINUE)
