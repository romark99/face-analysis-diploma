import os

from application.domain.model.executor.dataset.ChooseDatasetTypeExecutor import ChooseDatasetTypeExecutor
from definitions import DIR_DATA_FULL_PATH, CHOOSE_DATASET_TYPE, WRONG_OPTION_MESSAGE


class ChooseDatasetTypeExecutorConsoleImpl(ChooseDatasetTypeExecutor):
    def execute(self):
        while True:
            print(CHOOSE_DATASET_TYPE)
            i = 0
            files = os.listdir(DIR_DATA_FULL_PATH)
            dirs = [file for file in files if os.path.isdir(os.path.join(DIR_DATA_FULL_PATH, file))]
            dirs.sort()
            datasetTypesDict = dict()
            for typeDirName in dirs:
                i += 1
                print(f"'{i}' : {typeDirName}")
                datasetTypesDict[str(i)] = typeDirName
            choice = input()
            if choice in datasetTypesDict.keys():
                self.datasetType = datasetTypesDict[choice]
                return
            print(WRONG_OPTION_MESSAGE)
