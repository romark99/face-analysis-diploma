import os
from distutils.dir_util import copy_tree

from application.domain.model.executor.dataset.CopyDatasetExecutor import CopyDatasetExecutor
from definitions import DATASET_FOLDER_IS_NOT_FOUND, DATASET_WITH_SUCH_FORMAT_ALREADY_EXISTS, COPYING_DATASET_FOLDER, \
    ERROR_WHEN_COPYING_DATASET, DATASET_SUCCESSFULLY_ADDED


class CopyDatasetExecutorConsoleImpl(CopyDatasetExecutor):
    def execute(self, dataset, fromFolderPath):
        if not os.path.isdir(fromFolderPath):
            print(DATASET_FOLDER_IS_NOT_FOUND)
        if os.path.exists(dataset.path):
            print(DATASET_WITH_SUCH_FORMAT_ALREADY_EXISTS)
        else:
            print(COPYING_DATASET_FOLDER)
            try:
                copy_tree(fromFolderPath, dataset.path, preserve_symlinks=1)
            except:
                print(ERROR_WHEN_COPYING_DATASET)
                return
            print(DATASET_SUCCESSFULLY_ADDED)
