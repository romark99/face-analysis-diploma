import os

from application.domain.model.executor.dataset.ChooseDatasetNameByTypeExecutor import ChooseDatasetNameByTypeExecutor
from definitions import WRONG_OPTION_MESSAGE, CHOOSE_DATASET, DATASET_TYPE_FOLDER_IS_NOT_FOUND, \
    DATASETS_OF_FOLLOWING_TYPE_NOT_FOUND, ENTER_ANY_STRING_TO_CONTINUE, DIR_DATA_FULL_PATH, DIR_DATASET_NAME


class ChooseDatasetNameByTypeExecutorConsoleImpl(ChooseDatasetNameByTypeExecutor):
    def execute(self, datasetType):
        self.datasetPath = None
        while True:
            print(CHOOSE_DATASET)
            i = 0
            datasetTypePath = os.path.join(DIR_DATA_FULL_PATH, datasetType, DIR_DATASET_NAME)
            if not os.path.exists(datasetTypePath):
                print(DATASET_TYPE_FOLDER_IS_NOT_FOUND)
                input(ENTER_ANY_STRING_TO_CONTINUE)
                return
            files = os.listdir(datasetTypePath)
            dirs = list(filter(lambda file: os.path.isdir(os.path.join(datasetTypePath, file)), files))
            if not dirs:
                self.datasetName = None
                print(DATASETS_OF_FOLLOWING_TYPE_NOT_FOUND)
                input(ENTER_ANY_STRING_TO_CONTINUE)
                return
            dirs.sort()
            datasetNamesDict = dict()
            for typeDirName in dirs:
                i += 1
                print(f"'{i}' : {typeDirName}")
                datasetNamesDict[str(i)] = typeDirName
            choice = input()
            if choice in datasetNamesDict.keys():
                self.datasetName = datasetNamesDict[choice]
                return
            print(WRONG_OPTION_MESSAGE)
