import os

from application.domain.model.executor.training_results.SaveTextResultsExecutor import SaveTextResultsExecutor
from definitions import FILE_RESULTS_TXT


class SaveTextResultsExecutorTextFileImpl(SaveTextResultsExecutor):

    # best results
    # 1. Best accuracy
    # 2. Best epoch
    # 3. Number of epochs
    # 4. Time of epoch
    # 5. Total time
    def execute(self, kerasHistory, resultsFolderPath, totalTime):
        errorFilePath = os.path.join(resultsFolderPath, FILE_RESULTS_TXT)
        f = open(errorFilePath, 'w')
        best_accuracy = max(kerasHistory.history['val_acc'])
        f.write(f"Best accuracy: {best_accuracy * 100}%\n")
        f.write(f"Best epoch: {kerasHistory.history['val_acc'].index(best_accuracy) + 1}\n")
        numberOfEpochs = len(kerasHistory.history['val_acc'])
        f.write(f"Number of epochs: {numberOfEpochs}\n")
        f.write(f"Total time: {totalTime} s\n")
        f.write(f"Time per epoch (approximately): {totalTime / numberOfEpochs} s\n")
        f.close()
