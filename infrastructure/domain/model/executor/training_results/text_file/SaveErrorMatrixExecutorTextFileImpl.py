import os

import numpy as np

from application.domain.model.executor.training_results.SaveErrorMatrixExecutor import SaveErrorMatrixExecutor
from definitions import FILE_ERROR_MATRIX_TXT


class SaveErrorMatrixExecutorTextFileImpl(SaveErrorMatrixExecutor):
    def execute(self, kerasHistory, resultsFolderPath):
        y_predicted = kerasHistory.model.predict(kerasHistory.validation_data[0],
                                                 batch_size=kerasHistory.params['batch_size'])

        num_labels = kerasHistory.model.output_shape[1]
        mtx = np.zeros((num_labels, num_labels))

        sum1 = 0
        for i in range(len(y_predicted)):
            pred_label = np.argmax(y_predicted[i])
            et_label = np.argmax(kerasHistory.validation_data[1][i])
            mtx[pred_label][et_label] += 1
            if pred_label == et_label:
                sum1 += 1
        print(100 * sum1 / len(y_predicted))

        errorFilePath = os.path.join(resultsFolderPath, FILE_ERROR_MATRIX_TXT)
        f = open(errorFilePath, 'w')
        f.write("Matrix:\n")
        for i in range(num_labels):
            for j in range(num_labels):
                f.write(f'{str(int(mtx[i][j]))} ')
            f.write('\n')
        f.close()
