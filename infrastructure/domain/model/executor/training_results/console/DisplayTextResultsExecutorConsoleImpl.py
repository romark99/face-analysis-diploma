import os

from application.domain.model.executor.training_results.DisplayTextResultsExecutor import DisplayTextResultsExecutor
from definitions import FILE_RESULTS_TXT


class DisplayTextResultsExecutorConsoleImpl(DisplayTextResultsExecutor):
    def execute(self, resultsFolderPath):
        errorFilePath = os.path.join(resultsFolderPath, FILE_RESULTS_TXT)
        f = open(errorFilePath, "r")
        for x in f:
            print(x)
