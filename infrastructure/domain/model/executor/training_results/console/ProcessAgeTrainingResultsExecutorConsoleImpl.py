import numpy as np

from domain.model.dataset_format import DatasetFormat
from domain.model.executor.Executor import Executor


class ProcessAgeTrainingResultsExecutorConsoleImpl(Executor):
    def execute(self, datasetFormatName, kerasHistory, saveFolderPath, totalTime, imageNames):
        y_predicted = kerasHistory.model.predict(kerasHistory.validation_data[0],
                                                 batch_size=kerasHistory.params['batch_size'], verbose=1)
        y_etalon = kerasHistory.validation_data[1]

        testImageNames = imageNames[1]
        numLabels = 10
        ageCategoriesMatrix = np.zeros((numLabels, numLabels))

        if datasetFormatName == DatasetFormat.ENCODING_128_CSV:
            ages = np.arange(0, 116).reshape(116, 1)
            y_predicted = y_predicted.dot(ages).flatten()

        sum1 = 0
        for i in range(y_predicted.shape[0]):
            print('-------')
            print(testImageNames[i])
            if datasetFormatName == DatasetFormat.ENCODING_128_CSV_1_OUTPUT:
                testAge = int(round(y_etalon[i][0] * 116 + 1))
                predictedAge = int(round(y_predicted[i][0] * 116 + 1))
            else:  # if datasetFormatName == DatasetFormat.ENCODING_128_CSV:
                testAge = np.argmax(y_etalon[i]) + 1
                predictedAge = int(round(y_predicted[i] + 1))
            print(f'TST AGE: {testAge}')
            print(f'PRD AGE: {predictedAge}')
            print(f'DIFF: {testAge - predictedAge}')
            print('-------')
            gender = determineGenderByImageName(testImageNames[i])
            pred_label = determineAgeCategory(predictedAge, gender)
            et_label = determineAgeCategory(testAge, gender)
            ageCategoriesMatrix[pred_label][et_label] += 1
            if pred_label == et_label:
                sum1 += 1
        accuracy = 100 * sum1 / len(y_predicted)
        print(accuracy)
        print("Matrix:")
        for i in range(10):
            print(ageCategoriesMatrix[i])


def determineGenderByImageName(imageName):
    a = imageName.split('_')[1]
    if a == '0':
        return True
    return False


# Раннее детство — от 1 до 2 лет
# Первый период детства — от 3 до 7 лет
# Второй период детства — от 8 до 12 лет (муж.); от 8 до 11 лет (жен.)
# Подростковый возраст — от 13 до 16 лет (муж.); от 12 до 15 лет (жен.)
# Юношеский возраст — от 17 до 21 года (муж.); от 16 до 20 лет (жен.)
# Средний возраст

#    первый период — от 22 до 35 года (муж.); от 21 до 35 лет (жен.)
#    второй период — от 36 до 60 года (муж.); от 36 до 55 лет (жен.)

# Пожилые люди — от 61 до 75 года (муж.); от 56 до 75 лет (жен.)
# Старческий возраст — от 76 до 90 лет
# Долгожители — старше 90 лет

# 10

def determineAgeCategory(age, isMale):
    if age >= 1 and age <= 2:
        return 0
    elif age >= 3 and age <= 7:
        return 1
    elif age >= 76 and age <= 90:
        return 8
    elif age >= 91:
        return 9
    elif isMale:
        if age >= 8 and age <= 12:
            return 2
        elif age >= 13 and age <= 16:
            return 3
        elif age >= 17 and age <= 21:
            return 4
        elif age >= 22 and age <= 35:
            return 5
        elif age >= 36 and age <= 60:
            return 6
        elif age >= 61 and age <= 75:
            return 7
    else:
        if age >= 8 and age <= 11:
            return 2
        elif age >= 12 and age <= 15:
            return 3
        elif age >= 16 and age <= 20:
            return 4
        elif age >= 21 and age <= 35:
            return 5
        elif age >= 36 and age <= 55:
            return 6
        elif age >= 56 and age <= 75:
            return 7
    return -1
