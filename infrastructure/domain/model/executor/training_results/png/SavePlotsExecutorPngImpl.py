import os

import matplotlib.pyplot as plt
import numpy as np

from application.domain.model.executor.training_results.SavePlotsExecutor import SavePlotsExecutor
from definitions import FILE_LOSS_PNG, FILE_ACCURACY_PNG


class SavePlotsExecutorPngImpl(SavePlotsExecutor):
    def execute(self, kerasHistory, resultsFolderPath):
        self.saveLossPlot(kerasHistory, resultsFolderPath)
        self.saveAccuracyPlot(kerasHistory, resultsFolderPath)

    def saveLossPlot(self, kerasHistory, resultsFolderPath):
        plt.style.use("ggplot")
        plt.figure()
        N = len(kerasHistory.history["loss"])
        plt.plot(np.arange(0, N), kerasHistory.history["loss"], label="train_loss")
        plt.plot(np.arange(0, N), kerasHistory.history["val_loss"], label="val_loss")

        plt.title("Training/Test Loss")
        plt.xlabel("Epoch #")
        plt.ylabel("Loss")
        plt.legend(loc="upper right")

        lossPngPath = os.path.join(resultsFolderPath, FILE_LOSS_PNG)
        plt.savefig(lossPngPath)

    def saveAccuracyPlot(self, kerasHistory, resultsFolderPath):
        plt.style.use("ggplot")
        plt.figure()
        N = len(kerasHistory.history["acc"])
        plt.plot(np.arange(0, N), kerasHistory.history["acc"], label="train_accuracy")
        plt.plot(np.arange(0, N), kerasHistory.history["val_acc"], label="val_accuracy")

        plt.title("Training/Test Accuracy")
        plt.xlabel("Epoch #")
        plt.ylabel("Accuracy")
        plt.legend(loc="upper right")

        accuracyPngPath = os.path.join(resultsFolderPath, FILE_ACCURACY_PNG)
        plt.savefig(accuracyPngPath)
