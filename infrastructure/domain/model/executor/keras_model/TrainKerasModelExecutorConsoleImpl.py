import os
import time

from keras.callbacks import ModelCheckpoint, Callback

from application.domain.model.executor.keras_model.TrainKerasModelExecutor import TrainKerasModelExecutor


class TrainKerasModelExecutorConsoleImpl(TrainKerasModelExecutor):
    def execute(self, kerasModel, netData, netParams, saveModelPath):
        x_train, x_test, y_train, y_test = netData
        callbacks = self.createSaveCallbacks(saveModelPath)
        if netParams.earlyStopping is not None:
            callbacks.append(netParams.earlyStopping)

        startTime = time.time()
        self.kerasHistory = kerasModel.fit(x_train, y_train,
                                           batch_size=netParams.batchSize,
                                           epochs=netParams.epochs,
                                           verbose=1,
                                           validation_data=(x_test, y_test),
                                           shuffle=True,
                                           callbacks=callbacks)
        self.totalTime = time.time() - startTime

    def createSaveCallbacks(self, saveModelPath):
        os.mkdir(saveModelPath)

        # accPath = os.path.join(saveModelPath, 'model.best_accuracy-{epoch:03d}-{val_loss:.2f}-{val_acc:.4f}.h5')
        lossPath = os.path.join(saveModelPath, 'model.best_loss-{epoch:03d}-{val_loss:.2f}-{val_acc:.4f}.h5')

        # mcp_acc = ModelCheckpoint(accPath, save_best_only=True, monitor='val_acc', mode='max')
        mcp_loss = ModelCheckpoint(lossPath, save_best_only=True, save_weights_only=True, monitor='val_loss',
                                   mode='min')
        # callbacks = [mcp_acc, mcp_loss]
        callbacks = [mcp_loss, RemoveExtraModelCallback(saveModelPath)]
        return callbacks


class RemoveExtraModelCallback(Callback):
    def __init__(self, saveModelPath):
        self.saveModelPath = saveModelPath
        super().__init__()

    def on_epoch_end(self, epoch, logs=None):
        self.removeWorseModels()

    def removeWorseModels(self):
        models = os.listdir(self.saveModelPath)
        models.sort()
        if len(models) > 3:
            for modelName in models[:-3]:
                modelPath = os.path.join(self.saveModelPath, modelName)
                print(f'{modelName} removed!')
                os.remove(modelPath)
