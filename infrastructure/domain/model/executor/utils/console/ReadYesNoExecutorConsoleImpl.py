from application.domain.model.executor.utils.ReadYesNoExecutor import ReadYesNoExecutor
from definitions import WRONG_OPTION_MESSAGE


class ReadYesNoExecutorConsoleImpl(ReadYesNoExecutor):
    def __init__(self, title) -> None:
        self.title = title
        super().__init__()

    def execute(self):
        while True:
            print(self.title)
            print("'1' : Yes")
            print("'2' : No")
            choice = input()
            if choice == '1':
                self.result = True
                return
            elif choice == '2':
                self.result = False
                return
            else:
                print(WRONG_OPTION_MESSAGE)
