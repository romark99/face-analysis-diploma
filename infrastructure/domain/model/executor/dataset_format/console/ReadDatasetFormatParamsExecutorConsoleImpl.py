import json
import os

from application.domain.model.executor.dataset_format.ReadDatasetFormatParamsExecutor import \
    ReadDatasetFormatParamsExecutor
from definitions import FILE_DATASET_PARAMS_INFO


class ReadDatasetFormatParamsExecutorConsoleImpl(ReadDatasetFormatParamsExecutor):
    def execute(self, datasetPathWithoutFormat, chosenFormat):
        print('Reading dataset params...')
        paramFilePath = os.path.join(datasetPathWithoutFormat, chosenFormat, FILE_DATASET_PARAMS_INFO)
        with open(paramFilePath, 'r') as f:
            paramDict = json.load(f)
            self.inputShape = tuple(paramDict['inputShape'])
            self.numberOfClasses = int(paramDict['numberOfClasses'])
        print('Reading dataset params is finished.')
