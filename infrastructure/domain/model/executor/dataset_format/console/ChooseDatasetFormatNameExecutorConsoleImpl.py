from application.domain.model.executor.dataset_format.ChooseDatasetFormatNameExecutor import \
    ChooseDatasetFormatNameExecutor
from definitions import CHOOSE_DATASET_FORMAT, WRONG_OPTION_MESSAGE
from domain.model.dataset_format import DatasetFormat


class ChooseDatasetFormatNameExecutorConsoleImpl(ChooseDatasetFormatNameExecutor):

    def execute(self):
        while True:
            print(CHOOSE_DATASET_FORMAT)
            key = 1
            formatDict = dict()
            for datasetFormatName, datasetFormatDescription in DatasetFormat.DATASET_FORMAT_DESCRIPTIONS.items():
                print(f"'{key}' : {datasetFormatName} ({datasetFormatDescription})")
                formatDict[str(key)] = datasetFormatName
                key += 1
            choice = input()
            if choice in formatDict.keys():
                self.datasetFormatName = formatDict[choice]
                return
            print(WRONG_OPTION_MESSAGE)
