import os

from application.domain.model.executor.dataset_format.GetDatasetFormatsFromDatasetFolderExecutor import \
    GetDatasetFormatsFromDatasetFolderExecutor


class GetDatasetFormatsFromDatasetFolderExecutorConsoleImpl(GetDatasetFormatsFromDatasetFolderExecutor):
    def execute(self, datasetPath):
        self.datasetFormats = os.listdir(datasetPath)
