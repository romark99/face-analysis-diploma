from application.domain.model.executor.dataset_format.BuildDatasetFormatParamsExecutor import \
    BuildDatasetFormatParamsExecutor


class BuildDatasetFormatParamsExecutorConsoleImpl(BuildDatasetFormatParamsExecutor):
    def execute(self):
        self.inputShape = readInputShape()
        self.numberOfClasses = readNumberOfClasses()


def readInputShape():
    while True:
        try:
            print('Enter an input shape of dataset (a tuple of 1-3 integers; "*" means any): ')
            inputShape = list(x.strip() for x in input().split(','))
            if len(inputShape) == 0 or len(inputShape) > 3:
                raise ValueError
            inputShape = map(lambda shape: None if shape == '*' else int(shape), inputShape)
            return tuple(inputShape)
        except ValueError:
            print('Wrong value. Enter a valid tuple of 1-3 integers; "*" means any!')


def readNumberOfClasses():
    while True:
        try:
            print('Enter a number of classes (a positive integer): ')
            numberOfClasses = int(input())
            if numberOfClasses < 1:
                raise ValueError
            return numberOfClasses
        except ValueError:
            print('Wrong value. Enter a valid positive integer!')
