import json
import os

from application.domain.model.executor.dataset_format.SaveDatasetFormatParamsExecutor import \
    SaveDatasetFormatParamsExecutor
from definitions import FILE_DATASET_PARAMS_INFO, ENTER_ANY_STRING_TO_CONTINUE


class SaveDatasetFormatParamsExecutorConsoleImpl(SaveDatasetFormatParamsExecutor):
    def execute(self, datasetPathWithoutFormat, datasetFormat):
        print('Writing dataset params...')
        paramFilePath = os.path.join(datasetPathWithoutFormat, datasetFormat.name, FILE_DATASET_PARAMS_INFO)
        paramDict = {'inputShape': datasetFormat.inputShape, 'numberOfClasses': datasetFormat.numberOfClasses}
        with open(paramFilePath, 'w') as fp:
            json.dump(paramDict, fp)
        print('Writing dataset params is finished.')
        input(ENTER_ANY_STRING_TO_CONTINUE)
