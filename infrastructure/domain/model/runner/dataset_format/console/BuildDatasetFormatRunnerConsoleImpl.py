from application.domain.model.runner.dataset_format.BuildDatasetFormatRunner import BuildDatasetFormatRunner
from infrastructure.domain.model.executor.dataset_format.console.BuildDatasetFormatParamsExecutorConsoleImpl import \
    BuildDatasetFormatParamsExecutorConsoleImpl
from infrastructure.domain.model.executor.dataset_format.console.ChooseDatasetFormatNameExecutorConsoleImpl import \
    ChooseDatasetFormatNameExecutorConsoleImpl


class BuildDatasetFormatRunnerConsoleImpl(BuildDatasetFormatRunner):
    def __init__(self) -> None:
        super().__init__(ChooseDatasetFormatNameExecutorConsoleImpl(),
                         BuildDatasetFormatParamsExecutorConsoleImpl())
