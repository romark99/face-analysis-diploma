from shutil import copyfile

from application.domain.model.runner.dataset_format.ReformatDatasetFormatRunner import ReformatDatasetFormatRunner
from infrastructure.domain.model.executor.dataset_format.console.GetDatasetFormatsFromDatasetFolderExecutorConsoleImpl import \
    GetDatasetFormatsFromDatasetFolderExecutorConsoleImpl
from infrastructure.domain.model.utils.cropping.CroppingUtils import CroppingUtils
from infrastructure.domain.model.utils.reformatter.ReformatterUtils import *


class ReformatDatasetFormatRunnerConsoleImpl(ReformatDatasetFormatRunner):
    def run(self) -> None:
        getDatasetFormatsFromDatasetFolderExecutor = GetDatasetFormatsFromDatasetFolderExecutorConsoleImpl()

        getDatasetFormatsFromDatasetFolderExecutor.execute(self.datasetPathWithoutFormat)
        datasetFormats = getDatasetFormatsFromDatasetFolderExecutor.getDatasetFormats()

        setDatasetFormats = set(datasetFormats)

        toPath = os.path.join(self.datasetPathWithoutFormat, self.newFormatName)

        applicableFormats = set()
        if self.newFormatName == DatasetFormat.FOLDER_OF_LABELS_CSV:
            applicableFormats = {DatasetFormat.FOLDER_OF_LABELS_CROPPED}
        elif self.newFormatName == DatasetFormat.ENCODING_128_CSV_1_OUTPUT:
            applicableFormats = {DatasetFormat.ENCODING_128_CSV, DatasetFormat.FOLDER_OF_LABELS_WILD,
                                 DatasetFormat.FOLDER_OF_LABELS_CROPPED,
                                 DatasetFormat.FOLDER_OF_LABELS_CROPPED_BLACK}
        elif self.newFormatName == DatasetFormat.ENCODING_128_CSV:
            applicableFormats = {DatasetFormat.ENCODING_128_CSV_1_OUTPUT, DatasetFormat.FOLDER_OF_LABELS_WILD,
                                 DatasetFormat.FOLDER_OF_LABELS_CROPPED,
                                 DatasetFormat.FOLDER_OF_LABELS_CROPPED_BLACK}
        elif self.newFormatName == DatasetFormat.FOLDER_OF_LABELS_CROPPED:
            applicableFormats = {DatasetFormat.FOLDER_OF_LABELS_WILD}
        elif self.newFormatName == DatasetFormat.FOLDER_OF_LABELS_CROPPED_BLACK:
            applicableFormats = {DatasetFormat.FOLDER_OF_LABELS_CROPPED}
        elif self.newFormatName == DatasetFormat.FOLDER_OF_LABELS_CSV_BLACK:
            applicableFormats = {DatasetFormat.FOLDER_OF_LABELS_CROPPED_BLACK}

        format = chooseFromFormat(setDatasetFormats, applicableFormats)

        if format is None:
            self.newFormatName = None
            return
        fromPath = os.path.join(self.datasetPathWithoutFormat, format)
        os.mkdir(toPath)

        if self.newFormatName == DatasetFormat.FOLDER_OF_LABELS_CSV:
            fromBaseToFolderOfLabelsCsvFormat(fromPath, toPath)
            return
        elif self.newFormatName == DatasetFormat.ENCODING_128_CSV or self.newFormatName == DatasetFormat.ENCODING_128_CSV_1_OUTPUT:
            if format == DatasetFormat.ENCODING_128_CSV or format == DatasetFormat.ENCODING_128_CSV_1_OUTPUT:
                copyfile(os.path.join(fromPath, f'{DatasetFormat.ENCODING_128_CSV}.csv'),
                         os.path.join(toPath, f'{DatasetFormat.ENCODING_128_CSV}.csv'))
                input('Copying is finished.')
            else:
                fromBaseToEncoding128CsvFormat(fromPath, toPath)
            return
        elif self.newFormatName == DatasetFormat.FOLDER_OF_LABELS_CROPPED:
            utils = CroppingUtils()
            utils.fromWildToBaseFormat(fromPath, toPath, self.inputShape)
            return
        elif self.newFormatName == DatasetFormat.FOLDER_OF_LABELS_CROPPED_BLACK:
            fromBaseToBlackFormat(fromPath, toPath)
            return
        elif self.newFormatName == DatasetFormat.FOLDER_OF_LABELS_CSV_BLACK:
            fromBlackToCsvBlackFormat(fromPath, toPath)
            return

        self.newFormatName = None
        input('Following reformater is not implemented yet.')
