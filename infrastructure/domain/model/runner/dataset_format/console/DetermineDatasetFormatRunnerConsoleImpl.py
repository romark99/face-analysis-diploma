from application.domain.model.runner.dataset_format.DetermineDatasetFormatRunner import DetermineDatasetFormatRunner
from infrastructure.domain.model.executor.dataset_format.console.BuildDatasetFormatParamsExecutorConsoleImpl import \
    BuildDatasetFormatParamsExecutorConsoleImpl
from infrastructure.domain.model.executor.dataset_format.console.ChooseDatasetFormatNameExecutorConsoleImpl import \
    ChooseDatasetFormatNameExecutorConsoleImpl
from infrastructure.domain.model.executor.dataset_format.console.GetDatasetFormatsFromDatasetFolderExecutorConsoleImpl import \
    GetDatasetFormatsFromDatasetFolderExecutorConsoleImpl
from infrastructure.domain.model.executor.dataset_format.console.ReadDatasetFormatParamsExecutorConsoleImpl import \
    ReadDatasetFormatParamsExecutorConsoleImpl
from infrastructure.domain.model.executor.dataset_format.console.SaveDatasetFormatParamsExecutorConsoleImpl import \
    SaveDatasetFormatParamsExecutorConsoleImpl
from infrastructure.domain.model.executor.utils.console.ReadYesNoExecutorConsoleImpl import ReadYesNoExecutorConsoleImpl
from infrastructure.domain.model.runner.dataset_format.console.ReformatDatasetFormatRunnerConsoleImpl import \
    ReformatDatasetFormatRunnerConsoleImpl


class DetermineDatasetFormatRunnerConsoleImpl(DetermineDatasetFormatRunner):
    def __init__(self) -> None:
        super().__init__(ChooseDatasetFormatNameExecutorConsoleImpl(),
                         GetDatasetFormatsFromDatasetFolderExecutorConsoleImpl(),
                         ReadYesNoExecutorConsoleImpl(
                             'The following format does not exist for the dataset.\nDo you want to reformat dataset to a necessary format? '),
                         BuildDatasetFormatParamsExecutorConsoleImpl(),
                         ReformatDatasetFormatRunnerConsoleImpl(),
                         ReadDatasetFormatParamsExecutorConsoleImpl(),
                         SaveDatasetFormatParamsExecutorConsoleImpl())
