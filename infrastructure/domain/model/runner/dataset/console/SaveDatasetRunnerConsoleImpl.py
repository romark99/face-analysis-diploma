from application.domain.model.runner.dataset.SaveDatasetRunner import SaveDatasetRunner
from infrastructure.domain.model.executor.dataset.console.CopyDatasetExecutorConsoleImpl import \
    CopyDatasetExecutorConsoleImpl
from infrastructure.domain.model.executor.dataset_format.console.SaveDatasetFormatParamsExecutorConsoleImpl import \
    SaveDatasetFormatParamsExecutorConsoleImpl


class SaveDatasetRunnerConsoleImpl(SaveDatasetRunner):
    def __init__(self) -> None:
        super().__init__(CopyDatasetExecutorConsoleImpl(),
                         SaveDatasetFormatParamsExecutorConsoleImpl())
