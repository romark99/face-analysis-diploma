from application.domain.model.runner.dataset.DetermineDatasetRunner import DetermineDatasetRunner
from infrastructure.domain.model.executor.utils.console.ReadYesNoExecutorConsoleImpl import ReadYesNoExecutorConsoleImpl
from infrastructure.domain.model.runner.dataset.console.BuildDatasetRunnerConsoleImpl import \
    BuildDatasetRunnerConsoleImpl
from infrastructure.domain.model.runner.dataset.console.ChooseDatasetRunnerConsoleImpl import \
    ChooseDatasetRunnerConsoleImpl


class DetermineDatasetRunnerConsoleImpl(DetermineDatasetRunner):
    def __init__(self) -> None:
        super().__init__(ReadYesNoExecutorConsoleImpl('Do you want to choose an existing dataset?'),
                         BuildDatasetRunnerConsoleImpl(),
                         ChooseDatasetRunnerConsoleImpl())
