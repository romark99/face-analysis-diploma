from application.domain.model.runner.dataset.BuildDatasetRunner import BuildDatasetRunner
from infrastructure.domain.model.executor.dataset.console.ChooseDatasetTypeExecutorConsoleImpl import \
    ChooseDatasetTypeExecutorConsoleImpl
from infrastructure.domain.model.executor.dataset.dialog.ChooseDataFolderExecutorDialogImpl import \
    ChooseDataFolderExecutorDialogImpl
from infrastructure.domain.model.runner.dataset.console.SaveDatasetRunnerConsoleImpl import SaveDatasetRunnerConsoleImpl
from infrastructure.domain.model.runner.dataset_format.console.BuildDatasetFormatRunnerConsoleImpl import \
    BuildDatasetFormatRunnerConsoleImpl


class BuildDatasetRunnerConsoleImpl(BuildDatasetRunner):
    def __init__(self) -> None:
        super().__init__(ChooseDatasetTypeExecutorConsoleImpl(),
                         ChooseDataFolderExecutorDialogImpl(),
                         BuildDatasetFormatRunnerConsoleImpl(),
                         SaveDatasetRunnerConsoleImpl())
