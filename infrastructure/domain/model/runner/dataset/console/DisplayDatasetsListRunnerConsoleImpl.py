from application.domain.model.runner.dataset.DisplayDatasetsListRunner import DisplayDatasetsListRunner
from infrastructure.domain.model.executor.dataset.console.DisplayDatasetsListExecutorConsoleImpl import \
    DisplayDatasetsListExecutorConsoleImpl


class DisplayDatasetsListRunnerConsoleImpl(DisplayDatasetsListRunner):

    def __init__(self) -> None:
        super().__init__(DisplayDatasetsListExecutorConsoleImpl())
