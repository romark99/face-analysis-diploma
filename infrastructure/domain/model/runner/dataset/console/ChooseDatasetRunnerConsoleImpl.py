from application.domain.model.runner.dataset.ChooseDatasetRunner import ChooseDatasetRunner
from infrastructure.domain.model.executor.dataset.console.ChooseDatasetNameByTypeExecutorConsoleImpl import \
    ChooseDatasetNameByTypeExecutorConsoleImpl
from infrastructure.domain.model.executor.dataset.console.ChooseDatasetTypeExecutorConsoleImpl import \
    ChooseDatasetTypeExecutorConsoleImpl
from infrastructure.domain.model.runner.dataset_format.console.DetermineDatasetFormatRunnerConsoleImpl import \
    DetermineDatasetFormatRunnerConsoleImpl


class ChooseDatasetRunnerConsoleImpl(ChooseDatasetRunner):
    def __init__(self) -> None:
        super().__init__(ChooseDatasetTypeExecutorConsoleImpl(),
                         ChooseDatasetNameByTypeExecutorConsoleImpl(),
                         DetermineDatasetFormatRunnerConsoleImpl())
