from application.domain.model.runner.training_results.ProcessTrainingResultsRunner import ProcessTrainingResultsRunner
from infrastructure.domain.model.executor.training_results.console.DisplayTextResultsExecutorConsoleImpl import \
    DisplayTextResultsExecutorConsoleImpl
from infrastructure.domain.model.executor.training_results.png.SavePlotsExecutorPngImpl import SavePlotsExecutorPngImpl
from infrastructure.domain.model.executor.training_results.text_file.SaveErrorMatrixExecutorTextFileImpl import \
    SaveErrorMatrixExecutorTextFileImpl
from infrastructure.domain.model.executor.training_results.text_file.SaveTextResultsExecutorTextFileImpl import \
    SaveTextResultsExecutorTextFileImpl


class ProcessTrainingResultsRunnerConsoleImpl(ProcessTrainingResultsRunner):
    def __init__(self) -> None:
        super().__init__(SavePlotsExecutorPngImpl(),
                         SaveErrorMatrixExecutorTextFileImpl(),
                         SaveTextResultsExecutorTextFileImpl(),
                         DisplayTextResultsExecutorConsoleImpl())
