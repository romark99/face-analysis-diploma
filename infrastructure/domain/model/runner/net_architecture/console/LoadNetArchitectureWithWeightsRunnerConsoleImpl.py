import os

from application.domain.model.runner.net_architecture.LoadNetArchitectureWithWeightsRunner import \
    LoadNetArchitectureWithWeightsRunner
from definitions import DIR_DATA_FULL_PATH, DIR_MODEL_NAME
from infrastructure.domain.model.reader.net_weights.console.NetWeightsReaderConsoleImpl import \
    NetWeightsReaderConsoleImpl
from infrastructure.domain.model.repository.net_architecture.json.NetArchitectureRepositoryJSONImpl import \
    NetArchitectureRepositoryJSONImpl
from infrastructure.domain.model.repository.net_weights.h5.NetWeightsRepositoryH5Impl import NetWeightsRepositoryH5Impl
from infrastructure.domain.model.runner.net_architecture.console.ChooseNetArchitectureRunnerConsoleImpl import \
    ChooseNetArchitectureRunnerConsoleImpl
from infrastructure.domain.model.utils.reader.console import ConsoleReaderUtils


class LoadNetArchitectureWithWeightsRunnerConsoleImpl(LoadNetArchitectureWithWeightsRunner):
    def __init__(self) -> None:
        weightRepo = NetWeightsRepositoryH5Impl()
        archRepo = NetArchitectureRepositoryJSONImpl()
        super().__init__(NetWeightsReaderConsoleImpl(weightRepo),
                         weightRepo,
                         ChooseNetArchitectureRunnerConsoleImpl(archRepo),
                         archRepo)

    def chooseModel(self, models):
        if models is None or models == []:
            return None
        modelName = ConsoleReaderUtils.chooseConsoleVariant(models, 'Choose model: ')
        return modelName

    def loadWeightsToNetArchitecture(self, netArchitecture, name, weightModel):
        weightPath = os.path.join(DIR_DATA_FULL_PATH, netArchitecture.type, DIR_MODEL_NAME, name, weightModel)

        netArchitecture.kerasArchitecture.load_weights(weightPath)
        return netArchitecture
