from application.domain.model.runner.net_architecture.ChooseNetArchitectureRunner import \
    ChooseNetArchitectureRunner
from infrastructure.domain.model.utils.reader.console import ConsoleReaderUtils


class ChooseNetArchitectureRunnerConsoleImpl(ChooseNetArchitectureRunner):
    def readNetArchitectureName(self, architectureNames):
        archName = ConsoleReaderUtils.chooseConsoleVariant(architectureNames, 'Choose architecture: ')
        return archName

    def run(self, datasetType):
        print('Choosing net architecture...')
        super().run(datasetType)
        print('Choosing net architecture finished.')
