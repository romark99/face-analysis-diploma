from application.domain.model.runner.net_architecture.DisplayNetArchitecturesListRunner import \
    DisplayNetArchitecturesListRunner
from definitions import STAR_STRING, ENTER_ANY_STRING_TO_CONTINUE, DATASET_TYPES
from infrastructure.domain.model.utils.reader.console import ConsoleReaderUtils


class DisplayNetArchitecturesListRunnerConsoleImpl(DisplayNetArchitecturesListRunner):
    def run(self):
        print(STAR_STRING)
        super().run()
        print(STAR_STRING)
        input(ENTER_ANY_STRING_TO_CONTINUE)

    def displayArchitectures(self, architectures):
        archDict = dict(zip(DATASET_TYPES, [list() for _ in range(len(DATASET_TYPES))]))
        for arch in architectures:
            archDict[arch.type].append(arch.name)
        ConsoleReaderUtils.displayConsoleMultiDict(archDict, 'architectures')
