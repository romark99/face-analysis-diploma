from application.domain.model.runner.net_architecture.DetermineNetArchitectureRunner import \
    DetermineNetArchitectureRunner
from infrastructure.domain.model.runner.net_architecture.console.BuildNetArchitectureRunnerConsoleImpl import \
    BuildNetArchitectureRunnerConsoleImpl
from infrastructure.domain.model.runner.net_architecture.console.ChooseNetArchitectureRunnerConsoleImpl import \
    ChooseNetArchitectureRunnerConsoleImpl
from infrastructure.domain.model.utils.reader.console import ConsoleReaderUtils


class DetermineNetArchitectureRunnerConsoleImpl(DetermineNetArchitectureRunner):

    def run(self):
        isChooseNetArchitecture = self.determineIfToChooseNetArchitecture()

        if isChooseNetArchitecture:
            runner = ChooseNetArchitectureRunnerConsoleImpl(self.netArchitectureRepo)
            runner.run(self.datasetType)
        else:
            runner = BuildNetArchitectureRunnerConsoleImpl(self.netArchitectureRepo)
            runner.run(self.datasetType)

        self.netArchitecture = runner.getNetArchitecture()

    def determineIfToChooseNetArchitecture(self):
        return ConsoleReaderUtils.readConsoleYesNo(question='Do you want to choose an existing network architecture?')
