from application.domain.model.runner.net_architecture.BuildNetArchitectureRunner import BuildNetArchitectureRunner
from infrastructure.domain.model.reader.net_architecture.console.NetArchitectureReaderConsoleImpl import \
    NetArchitectureReaderConsoleImpl


class BuildNetArchitectureRunnerConsoleImpl(BuildNetArchitectureRunner):
    def __init__(self, netArchitectureRepo) -> None:
        super().__init__(NetArchitectureReaderConsoleImpl(), netArchitectureRepo)
