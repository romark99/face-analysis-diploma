from domain.model.runner.Runner import Runner


class DisplayProgramInfoRunnerConsoleImpl(Runner):
    def run(self) -> None:
        input('''
Diploma project: 'Neural network system of face analysis.'
Author: Markovski Roman
Group: PO-1
''')
