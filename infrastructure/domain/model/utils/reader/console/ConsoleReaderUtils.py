from definitions import WRONG_OPTION_MESSAGE


def arrayToConsoleDict(arr, adderDict=None):
    dictionary = dict()
    i = 0
    for elem in arr:
        i += 1
        dictionary[str(i)] = elem
    if adderDict is not None:
        dictionary.update(adderDict)
    return dictionary


def chooseConsoleVariant(arr, choserMessage, defaultValue=None, adderDict=None):
    if arr is None or arr == []:
        print("No options applicable :(")
        return None
    dictionary = arrayToConsoleDict(arr, adderDict)
    while True:
        print(choserMessage)
        for key, value in dictionary.items():
            print(f"'{key}' : {value}")
        value = input()
        if defaultValue is not None and value == "":
            return defaultValue
        elif value in dictionary.keys():
            return dictionary[value]
        print(WRONG_OPTION_MESSAGE)


def displayConsoleMultiDict(archDict, word):
    for key, arr in archDict.items():
        print(f'- {key} {word}:')
        for elem in arr:
            print(f'----- {elem}')


def readConsoleYesNo(question):
    while True:
        print(question)
        print("'1' : Yes")
        print("'2' : No")
        choice = input()
        if choice == '1':
            return True
        elif choice == '2':
            return False
        else:
            print(WRONG_OPTION_MESSAGE)
