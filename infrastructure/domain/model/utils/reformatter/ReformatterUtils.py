import csv
import os

import cv2
import face_recognition
import numpy as np

from definitions import WRONG_OPTION_MESSAGE
from domain.model.dataset_format import DatasetFormat


def chooseFromFormat(setDatasetFormats, applicableFormats):
    while True:
        formats = list(setDatasetFormats.intersection(applicableFormats))
        key = 1
        print('Choose the format you want to use as base:')
        if formats is None or len(formats) == 0:
            input('No fitting reformatters are found.')
            return None
        formatDict = dict()
        for datasetFormatName in formats:
            print(f"'{key}' : {datasetFormatName}")
            formatDict[str(key)] = datasetFormatName
            key += 1
        choice = input()
        if choice in formatDict.keys():
            return formatDict[choice]
        print(WRONG_OPTION_MESSAGE)


def fromBaseToBlackFormat(fromPath, toPath):
    label_dirs = [dir for dir in os.listdir(fromPath) if os.path.isdir(os.path.join(fromPath, dir))]
    for label_dir in label_dirs:
        label_path = os.path.join(fromPath, label_dir)

        to_label_path = os.path.join(toPath, label_dir)
        os.mkdir(to_label_path)

        for image_name in os.listdir(label_path):
            image_path = os.path.join(label_path, image_name)
            image = cv2.imread(image_path)
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            cv2.imwrite(os.path.join(to_label_path, image_name), gray)


def fromBaseToEncoding128CsvFormatTest(path):
    all_encodings = []
    n = 0
    for image_name in os.listdir(path):
        image_path = os.path.join(path, image_name)

        if not (image_name.endswith('.png') or image_name.endswith('.jpg') or image_name.endswith('.jpeg')):
            continue

        # read input image
        image = face_recognition.load_image_file(image_path)
        if image is None:
            print("Could not read input image")
            # exit()
            continue
        encodings = face_recognition.face_encodings(image)
        n += 1
        print("# ", n)
        print(image_name)
        if len(encodings) > 0:
            arr = [image_name]
            arr.extend(encodings[0])
            all_encodings.append(arr)
            print("Faces detected: ", len(encodings))
        else:
            print("No face detected.")

    print("Number of pictures: ", len(all_encodings))

    with open(os.path.join(path, f'{DatasetFormat.ENCODING_128_CSV}.csv'), "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerows(all_encodings)


def fromBaseToEncoding128CsvFormat(fromPath, toPath):
    all_encodings = []
    n = 0

    label_dirs = [dir for dir in os.listdir(fromPath) if os.path.isdir(os.path.join(fromPath, dir))]
    for label_dir in label_dirs:
        label_path = os.path.join(fromPath, label_dir)

        for image_name in os.listdir(label_path):
            image_path = os.path.join(label_path, image_name)

            if not (image_name.endswith('.png') or image_name.endswith('.jpg') or image_name.endswith('.jpeg')):
                continue

            # read input image
            image = face_recognition.load_image_file(image_path)
            if image is None:
                print("Could not read input image")
                # exit()
                continue
            encodings = face_recognition.face_encodings(image)
            n += 1
            print("# ", n)
            print(image_name, label_dir)
            if len(encodings) > 0:
                arr = [image_name, label_dir]
                arr.extend(encodings[0])
                all_encodings.append(arr)
                print("Faces detected: ", len(encodings))
            else:
                print("No face detected.")

    print("Number of pictures: ", len(all_encodings))

    with open(os.path.join(toPath, f'{DatasetFormat.ENCODING_128_CSV}.csv'), "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerows(all_encodings)


def fromBlackToCsvBlackFormat(fromPath, toPath):
    def resizeImage(path, label):
        img = cv2.imread(path, cv2.COLOR_BGR2GRAY)
        img = img.flatten()
        print(label)
        print(img)
        return label, img

    with open(os.path.join(toPath, f'{DatasetFormat.FOLDER_OF_LABELS_CSV_BLACK}.csv'), 'w', newline='') as csvfile:
        csvfile.write('label,pixels\n')
        csvwriter = csv.writer(csvfile, delimiter=' ',
                               quotechar='|', quoting=csv.QUOTE_MINIMAL)
        dirs = [dir for dir in os.listdir(fromPath) if os.path.isdir(os.path.join(fromPath, dir))]
        for dir in dirs:
            curr_folder = fromPath + '/' + dir
            for file in os.listdir(curr_folder):
                if file.endswith(".png"):
                    print(file)
                    curr_file = curr_folder + '/' + file
                    label, img = resizeImage(curr_file, dir)
                    csvfile.write(label + ',')
                    csvwriter.writerow(img)


def fromBaseToFolderOfLabelsCsvFormat(fromPath, toPath):
    def resizeImage(path, label):
        img = cv2.imread(path, cv2.IMREAD_UNCHANGED)
        img = img.flatten()
        print(label)
        print(img)
        return label, img

    with open(os.path.join(toPath, f'{DatasetFormat.FOLDER_OF_LABELS_CSV}.csv'), 'w', newline='') as csvfile:
        csvfile.write('label,pixels\n')
        csvwriter = csv.writer(csvfile, delimiter=' ',
                               quotechar='|', quoting=csv.QUOTE_MINIMAL)
        dirs = [dir for dir in os.listdir(fromPath) if os.path.isdir(os.path.join(fromPath, dir))]
        for dir in dirs:
            curr_folder = fromPath + '/' + dir
            for file in os.listdir(curr_folder):
                if file.endswith(".png"):
                    print(file)
                    curr_file = curr_folder + '/' + file
                    label, img = resizeImage(curr_file, dir)
                    csvfile.write(label + ',')
                    csvwriter.writerow(img)


def fromCroppedArrayToGrayArray(croppedImageArray):
    croppedImageArray = croppedImageArray.astype(np.uint8)
    gray = cv2.cvtColor(croppedImageArray, cv2.COLOR_BGR2GRAY)
    return gray


def fromGrayArrayToGrayCsvArray(grayImageArray):
    return grayImageArray.flatten()


def fromWildToEncoding128CsvArray(imagePath, inputShape):
    imageName = os.path.basename(imagePath)
    if not (imageName.endswith('.png') or imageName.endswith('.jpg') or imageName.endswith('.jpeg')):
        print("The file is not an image")
        return None

    # read input image
    image = face_recognition.load_image_file(imagePath)
    if image is None:
        print("Could not read input image")
        # exit()
        return None
    encodings = face_recognition.face_encodings(image)
    if len(encodings) > 0:
        print("Faces detected: ", len(encodings))
        return encodings[0]
    else:
        print("No face detected.")
        return None
