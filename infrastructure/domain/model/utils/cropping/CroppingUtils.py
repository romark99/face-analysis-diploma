import os
import random
from time import sleep

import imageio
import numpy as np
import skimage.transform as sige
import tensorflow as tf

from infrastructure.domain.model.utils.cropping import detect_face, facenet

gpu_memory_fraction = 1.0
margin = 44
random_order = False


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class CroppingUtils(metaclass=Singleton):
    def __init__(self):
        self.pnet = None
        self.rnet = None
        self.onet = None
        self.loadDetectionModel()

    def loadDetectionModel(self):
        print('Creating networks...')
        sleep(random.random())
        with tf.Graph().as_default():
            gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)
            sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
            with sess.as_default():
                self.pnet, self.rnet, self.onet = detect_face.create_mtcnn(sess, None)

    def fromWildToBaseFormat(self, fromPath, toPath, imageSize):
        sleep(random.random())
        output_dir = os.path.expanduser(toPath)
        path_dict = {}
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        # Store some git revision info in a text file in the log directory
        dataset = facenet.get_dataset(fromPath)

        print('Creating networks and loading parameters')

        minsize = 20  # minimum size of face
        threshold = [0.6, 0.7, 0.7]  # three steps's threshold
        factor = 0.709  # scale factor

        nrof_images_total = 0
        nrof_successfully_aligned = 0
        if random_order:
            random.shuffle(dataset)
        for cls in dataset:
            output_class_dir = os.path.join(output_dir, cls.name)
            path_dict[cls.name] = output_class_dir
            if not os.path.exists(output_class_dir):
                os.makedirs(output_class_dir)
                if random_order:
                    random.shuffle(cls.image_paths)
            for image_path in cls.image_paths:
                nrof_images_total += 1
                filename = os.path.splitext(os.path.split(image_path)[1])[0]
                output_filename = os.path.join(output_class_dir, filename + '.png')
                print(image_path)
                if not os.path.exists(output_filename):
                    try:
                        img = imageio.imread(image_path)
                    except (IOError, ValueError, IndexError) as e:
                        errorMessage = '{}: {}'.format(image_path, e)
                        print(errorMessage)
                    else:
                        if img.ndim < 2:
                            print('Unable to align "%s"' % image_path)
                            continue
                        if img.ndim == 2:
                            img = facenet.to_rgb(img)
                        img = img[:, :, 0:3]

                        bounding_boxes, _ = detect_face.detect_face(img, minsize, self.pnet, self.rnet, self.onet,
                                                                    threshold, factor)
                        nrof_faces = bounding_boxes.shape[0]
                        if nrof_faces > 0:
                            det = bounding_boxes[:, 0:4]
                            det_arr = []
                            img_size = np.asarray(img.shape)[0:2]
                            if nrof_faces > 1:
                                bounding_box_size = (det[:, 2] - det[:, 0]) * (det[:, 3] - det[:, 1])
                                img_center = img_size / 2
                                offsets = np.vstack([(det[:, 0] + det[:, 2]) / 2 - img_center[1],
                                                     (det[:, 1] + det[:, 3]) / 2 - img_center[0]])
                                offset_dist_squared = np.sum(np.power(offsets, 2.0), 0)
                                index = np.argmax(
                                    bounding_box_size - offset_dist_squared * 2.0)  # some extra weight on the centering
                                det_arr.append(det[index, :])
                            else:
                                det_arr.append(np.squeeze(det))

                            for i, det in enumerate(det_arr):
                                det = np.squeeze(det)
                                bb = np.zeros(4, dtype=np.int32)
                                bb[0] = np.maximum(det[0] - margin / 2, 0)
                                bb[1] = np.maximum(det[1] - margin / 2, 0)
                                bb[2] = np.minimum(det[2] + margin / 2, img_size[1])
                                bb[3] = np.minimum(det[3] + margin / 2, img_size[0])
                                cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
                                # scaled = sige.resize(cropped, (args.image_size, args.image_size), interp='bilinear')
                                scaled = sige.resize(cropped, imageSize)
                                nrof_successfully_aligned += 1
                                filename_base, file_extension = os.path.splitext(output_filename)
                                output_filename_n = "{}{}".format(filename_base, file_extension)
                                imageio.imwrite(output_filename_n, scaled)
                        else:
                            print('Unable to align "%s"' % image_path)

        print('Total number of images: %d' % nrof_images_total)
        print('Number of successfully aligned images: %d' % nrof_successfully_aligned)

    # def fromWildToCropped(self, imagePath, imageSize):
    #     try:
    #         img = imageio.imread(imagePath)
    #     except (IOError, ValueError, IndexError) as e:
    #         errorMessage = '{}: {}'.format(imagePath, e)
    #         print(errorMessage)
    #     else:
    #         croppedArray = self.fromWildToCroppedArray(img, imageSize)
    #         imageName = os.path.basename(imagePath)
    #         path = os.path.dirname(imagePath)
    #         output_filename = os.path.join(path, f'{imageName}_cropped.png')
    #         filename_base, file_extension = os.path.splitext(output_filename)
    #         output_filename_n = f"{filename_base}{file_extension}"
    #         imageio.imwrite(output_filename_n, croppedArray)

    def fromWildToCroppedArray(self, imagePath, imageSize):
        # print('Loading parameters...')
        minsize = 20  # minimum size of face
        threshold = [0.6, 0.7, 0.7]  # three steps's threshold
        factor = 0.709  # scale factor
        try:
            img = imageio.imread(imagePath)
        except (IOError, ValueError, IndexError) as e:
            errorMessage = '{}: {}'.format(imagePath, e)
            print(errorMessage)
        else:

            if img.ndim < 2:
                # print('Unable to align.')
                return
            if img.ndim == 2:
                img = facenet.to_rgb(img)
            img = img[:, :, 0:3]

            bounding_boxes, _ = detect_face.detect_face(img, minsize, self.pnet, self.rnet, self.onet, threshold,
                                                        factor)
            nrof_faces = bounding_boxes.shape[0]
            if nrof_faces > 0:
                det = bounding_boxes[:, 0:4]
                det_arr = []
                img_size = np.asarray(img.shape)[0:2]
                if nrof_faces > 1:
                    bounding_box_size = (det[:, 2] - det[:, 0]) * (det[:, 3] - det[:, 1])
                    img_center = img_size / 2
                    offsets = np.vstack([(det[:, 0] + det[:, 2]) / 2 - img_center[1],
                                         (det[:, 1] + det[:, 3]) / 2 - img_center[0]])
                    offset_dist_squared = np.sum(np.power(offsets, 2.0), 0)
                    index = np.argmax(
                        bounding_box_size - offset_dist_squared * 2.0)  # some extra weight on the centering
                    det_arr.append(det[index, :])
                else:
                    det_arr.append(np.squeeze(det))

                for i, det in enumerate(det_arr):
                    det = np.squeeze(det)
                    bb = np.zeros(4, dtype=np.int32)
                    bb[0] = np.maximum(det[0] - margin / 2, 0)
                    bb[1] = np.maximum(det[1] - margin / 2, 0)
                    bb[2] = np.minimum(det[2] + margin / 2, img_size[1])
                    bb[3] = np.minimum(det[3] + margin / 2, img_size[0])
                    cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
                    scaled = sige.resize(cropped, imageSize, preserve_range=True)
                    # print('Number of successfully aligned images: 1')
                    return scaled
        # print('Number of successfully aligned images: 0')
        return None
