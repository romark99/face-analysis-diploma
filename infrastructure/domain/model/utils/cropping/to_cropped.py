import os

from definitions import ROOT_DIR_FULL_PATH
from infrastructure.domain.model.utils.cropping.CroppingUtils import CroppingUtils

utils = CroppingUtils()
utils.fromWildToCropped(os.path.join(ROOT_DIR_FULL_PATH, 'test_data', 'from_UTKFace_39', 'r5.jpg'), (48, 48, 3))
