import numpy as np

from definitions import IS_USE_AGE_WITH_1_OUTPUT

EMOTIONS = ['anger', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']
GENDERS = ['M', 'W']

# Раннее детство — от 1 до 2 лет
# Первый период детства — от 3 до 7 лет
# Второй период детства — от 8 до 12 лет (муж.); от 8 до 11 лет (жен.)
# Подростковый возраст — от 13 до 16 лет (муж.); от 12 до 15 лет (жен.)
# Юношеский возраст — от 17 до 21 года (муж.); от 16 до 20 лет (жен.)
# Средний возраст

#    первый период — от 22 до 35 года (муж.); от 21 до 35 лет (жен.)
#    второй период — от 36 до 60 года (муж.); от 36 до 55 лет (жен.)

# Пожилые люди — от 61 до 75 года (муж.); от 56 до 75 лет (жен.)
# Старческий возраст — от 76 до 90 лет
# Долгожители — старше 90 лет

# 10
AGE_CATEGORIES = ['I. Toddler', 'II. Child', 'III. Pre-teen', 'IV. Teenager', 'V. Youth', 'VI. Young adult',
                  'VII. Middle adult', 'VIII. Senior', 'IX. Old', 'X. Oldest']


def getSemanticPrediction(datasetType, value):
    if datasetType == 'age':
        if IS_USE_AGE_WITH_1_OUTPUT:
            age = int(round(value[0] * 116 + 1))
        else:
            age = int(round(value)) + 1
        return str(age)
    elif datasetType == 'emotion':
        label = int(np.argmax(value))
        return EMOTIONS[label]
    elif datasetType == 'face':
        pass
    elif datasetType == 'gender':
        label = int(np.argmax(value))
        return GENDERS[label]


def getAgeIndex(age, gender):
    age = int(age)
    isMale = gender == 'M'
    if age >= 1 and age <= 2:
        return 0
    elif age >= 3 and age <= 7:
        return 1
    elif age >= 76 and age <= 90:
        return 8
    elif age >= 91:
        return 9
    elif isMale:
        if age >= 8 and age <= 12:
            return 2
        elif age >= 13 and age <= 16:
            return 3
        elif age >= 17 and age <= 21:
            return 4
        elif age >= 22 and age <= 35:
            return 5
        elif age >= 36 and age <= 60:
            return 6
        elif age >= 61 and age <= 75:
            return 7
    else:
        if age >= 8 and age <= 11:
            return 2
        elif age >= 12 and age <= 15:
            return 3
        elif age >= 16 and age <= 20:
            return 4
        elif age >= 21 and age <= 35:
            return 5
        elif age >= 36 and age <= 55:
            return 6
        elif age >= 56 and age <= 75:
            return 7
    return -1


def getAgeCategory(age, gender):
    inx = getAgeIndex(age, gender)
    if inx != None:
        return AGE_CATEGORIES[inx]
    else:
        return "-"
