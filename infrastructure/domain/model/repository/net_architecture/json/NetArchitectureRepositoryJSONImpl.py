import os

from keras.models import model_from_json
from keras.utils import plot_model

from definitions import DIR_ARCHITECTURE_NAME, DIR_DATA_FULL_PATH
from domain.model.net_architecture.NetArchitecture import NetArchitecture
from domain.model.net_architecture.repository.NetArchitectureRepository import NetArchitectureRepository


class NetArchitectureRepositoryJSONImpl(NetArchitectureRepository):
    def getEntities(self, specification=None):
        return super().getEntities(specification)

    def readAllOnlyIds(self):
        architectures = []
        dirs = os.listdir(DIR_DATA_FULL_PATH)
        dirs = [file for file in dirs if os.path.isdir(os.path.join(DIR_DATA_FULL_PATH, file))]
        dirs.sort()
        for type in dirs:

            typeDirFullPath = os.path.join(DIR_DATA_FULL_PATH, type, DIR_ARCHITECTURE_NAME)
            names = os.listdir(typeDirFullPath)
            names.sort()
            for name in names:
                if name.endswith('.json'):
                    name = name[:-5]
                architecture = NetArchitecture(type, name)
                architectures.append(architecture)
        return architectures

    def add(self, entity):
        kerasArchitecture = entity.getKerasArchitecture()
        json_string = kerasArchitecture.to_json()
        architecturePath = os.path.join(DIR_DATA_FULL_PATH, entity.type, DIR_ARCHITECTURE_NAME,
                                        f'{entity.name}.json')
        with open(architecturePath, 'w', encoding='utf-8') as f:
            f.write(json_string)
        plot_model(kerasArchitecture,
                   to_file=os.path.join(DIR_DATA_FULL_PATH, entity.type, DIR_ARCHITECTURE_NAME, f'{entity.name}.png'),
                   show_shapes=True)
        self.refreshEntities()
        return entity

    def readFullEntity(self):
        raise NotImplementedError("'readFullEntity()' method should be overridden.")

    def determineKerasArchitecture(self, netArchitecture):
        archPath = os.path.join(DIR_DATA_FULL_PATH, netArchitecture.type, DIR_ARCHITECTURE_NAME,
                                netArchitecture.name)
        with open(f'{archPath}.json', 'r') as file:
            archJsonString = file.read()
            netArchitecture.kerasArchitecture = model_from_json(archJsonString)
            return netArchitecture
