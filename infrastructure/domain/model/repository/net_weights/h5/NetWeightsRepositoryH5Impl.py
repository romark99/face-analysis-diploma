import os

from definitions import DIR_DATA_FULL_PATH, DIR_MODEL_NAME
from domain.model.net_weights.NetWeights import NetWeights
from domain.model.net_weights.repository.NetWeightsRepository import NetWeightsRepository


class NetWeightsRepositoryH5Impl(NetWeightsRepository):

    def readAllOnlyIds(self):
        netWeightsList = []
        dirs = os.listdir(DIR_DATA_FULL_PATH)
        dirs = [file for file in dirs if os.path.isdir(os.path.join(DIR_DATA_FULL_PATH, file))]
        dirs.sort()
        for type in dirs:
            typeDirFullPath = os.path.join(DIR_DATA_FULL_PATH, type, DIR_MODEL_NAME)
            names = os.listdir(typeDirFullPath)
            names.sort()
            for name in names:
                netWeights = NetWeights(type, name)
                netWeightsList.append(netWeights)
        return netWeightsList

    def determineModels(self, entity):
        weightDirPath = os.path.join(DIR_DATA_FULL_PATH, entity.type, DIR_MODEL_NAME, entity.getName())
        files = os.listdir(weightDirPath)
        files = [file for file in files if file.endswith('.h5') or file.endswith('.hdf5')]
        files.sort()
        if len(files) == 0 or files is None:
            print("There are no saved models.")
            entity.models = None
        entity.models = files
