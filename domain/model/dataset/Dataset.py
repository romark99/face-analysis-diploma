import os

from definitions import DIR_DATASET_NAME, DIR_DATA_FULL_PATH


class Dataset:
    def __init__(self) -> None:
        self.path = None
        self.name = None
        # Emotion, gender, faces
        self.type = None
        self.format = None
        super().__init__()

    def getPathWithoutFormat(self):
        return os.path.join(DIR_DATA_FULL_PATH, self.type, DIR_DATASET_NAME, self.name)

    def determinePath(self):
        self.path = os.path.join(DIR_DATA_FULL_PATH, self.type, DIR_DATASET_NAME, self.name, self.format.name)
