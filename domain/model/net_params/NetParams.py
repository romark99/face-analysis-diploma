from keras.callbacks import EarlyStopping
from keras.optimizers import SGD, Adam, RMSprop

DEFAULT_BATCH_SIZE = 64
DEFAULT_EPOCHS = 100
DEFAULT_PATIENCE = 10


class NetParams:

    def __init__(self, batchSize=None, epochs=None, loss=None, earlyStopping=None, optimizer=None) -> None:
        self.batchSize = batchSize
        self.epochs = epochs
        self.loss = loss
        self.earlyStopping = earlyStopping
        self.optimizer = optimizer
        super().__init__()

    @staticmethod
    def buildOptimizer(optimizerName, alpha):
        if optimizerName == 'SGD':
            return SGD(lr=alpha)
        elif optimizerName == 'RMSProp':
            return RMSprop(lr=alpha)
        elif optimizerName == 'Adam':
            return Adam(lr=alpha)

    @staticmethod
    def buildEarlyStopping(monitor, patience):
        return EarlyStopping(monitor=monitor, patience=patience, min_delta=0.00005)

    @classmethod
    def from_json(cls, data):
        return cls(**data)

    @staticmethod
    def validateBatchSize(value):
        validationErrorMessage = f'"BatchSize" field value is not valid. Allowed value: positive integer.'
        try:
            if value == "":
                return DEFAULT_BATCH_SIZE, None
            value = int(value)
            if value <= 0:
                return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage

    @staticmethod
    def validateEpochs(value):
        validationErrorMessage = f'"Epochs" field value is not valid. Allowed value: positive integer.'
        try:
            if value == "":
                return DEFAULT_EPOCHS, None
            value = int(value)
            if value <= 0:
                return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage

    @staticmethod
    def validateEarlyStoppingPatience(value):
        validationErrorMessage = f'"EarlyStopping.Patience" field value is not valid. Allowed value: positive integer.'
        try:
            if value == "":
                return DEFAULT_PATIENCE, None
            value = int(value)
            if value <= 0:
                return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage

    @staticmethod
    def validateOptimizerAlpha(value, defaultAlpha):
        validationErrorMessage = f'"Optimizer.Alpha" field value is not valid. Allowed value: positive float.'
        try:
            if value == "":
                return defaultAlpha, None
            value = float(value)
            if value <= 0:
                return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage
