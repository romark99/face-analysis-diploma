from domain.model.net_params.NetParams import NetParams
from domain.model.reader.Reader import Reader


class NetParamsReader(Reader):
    def readBatchSize(self):
        raise NotImplementedError("'readBatchSize()' method should be overridden.")

    def readEpochs(self):
        raise NotImplementedError("'readEpochs()' method should be overridden.")

    def readLoss(self):
        raise NotImplementedError("'readLoss()' method should be overridden.")

    def isToUseEarlyStopping(self):
        raise NotImplementedError("'isToUseEarlyStopping()' method should be overridden.")

    def readEarlyStopping(self):
        monitor = self.readEarlyStoppingMonitor()
        if monitor == "":
            monitor = "val_loss"
        patience = self.readEarlyStoppingPatience()
        if patience == "":
            patience = 100
        return NetParams.buildEarlyStopping(monitor, patience)

    def readEarlyStoppingMonitor(self):
        raise NotImplementedError("'readEarlyStoppingMonitor()' method should be overridden.")

    def readEarlyStoppingPatience(self):
        raise NotImplementedError("'readEarlyStoppingPatience()' method should be overridden.")

    def readOptimizer(self):
        name = self.readOptimizerName()
        if name == "":
            name = "SGD"
        alpha = self.readOptimizerAlpha(name)
        return NetParams.buildOptimizer(name, alpha)

    def readOptimizerName(self):
        raise NotImplementedError("'readOptimizerName()' method should be overridden.")

    def readOptimizerAlpha(self, optimizerName):
        raise NotImplementedError("'readOptimizerAlpha()' method should be overridden.")

    def read(self):
        entity = NetParams()
        entity.batchSize = self.readBatchSize()
        entity.epochs = self.readEpochs()
        entity.loss = self.readLoss()

        if self.isToUseEarlyStopping():
            entity.earlyStopping = self.readEarlyStopping()
        else:
            entity.earlyStopping = None
        entity.optimizer = self.readOptimizer()
        return entity
