class Repository:
    def __init__(self):
        self.entities = []

    def getEntities(self, specification=None):
        self.refreshEntities()
        if specification is None:
            return self.entities
        else:
            filteredEntities = []
            specificDict = specification.__dict__
            specificDict = {k: v for k, v in specificDict.items() if v is not None}
            for entity in self.entities:
                isEqual = True
                for field, value in specificDict.items():
                    if value != getattr(entity, field):
                        isEqual = False
                        break
                if isEqual:
                    filteredEntities.append(entity)
            return filteredEntities

    def getEntity(self, specification):
        entities = self.getEntities(specification)
        if len(entities) > 1:
            raise KeyError("Too much entities under the same key.")
        elif len(entities) == 0:
            raise KeyError("No entities found.")
        return entities[0]

    def refreshEntities(self):
        self.entities = self.readAllOnlyIds()

    def readAllOnlyIds(self):
        raise NotImplementedError("'readAllOnlyIds()' method in Repository should be overridden.")

    def add(self, entity):
        raise NotImplementedError("'add()' method in Repository should be overridden.")
