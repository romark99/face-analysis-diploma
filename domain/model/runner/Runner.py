class Runner:
    def run(self, *args, **kwargs) -> None:
        raise NotImplementedError("'Run()' method should be overridden.")
