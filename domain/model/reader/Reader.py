class Reader:
    def read(self, *args, **kwargs):
        raise NotImplementedError("'read()' method should be overridden.")
