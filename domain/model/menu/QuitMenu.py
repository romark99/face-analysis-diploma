from domain.model.menu.Menu import Menu


class QuitMenu(Menu):
    CLOSE_CHAR = 'q'
    CLOSE_LABEL = 'Quit'
    BACK_CHAR = 'b'
    BACK_LABEL = 'Back'

    def __init__(self, title, choices):
        self.title = title
        self.choices = choices
