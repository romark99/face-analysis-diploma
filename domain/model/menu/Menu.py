from domain.model.runner.Runner import Runner


class Menu(Runner):
    CHOOSE_OPTION = 'Please, choose one of the following options:'

    def __init__(self, title, choices):
        self.title = title
        self.choices = choices