class Executor:
    def execute(self, *args, **kwargs):
        raise NotImplementedError("'Execute()' method should be overridden.")
