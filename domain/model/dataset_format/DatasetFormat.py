class DatasetFormat:
    def __init__(self) -> None:
        self.name = None
        self.description = None
        self.inputShape = None
        self.numberOfClasses = None
        super().__init__()

    def determineDescription(self):
        self.description = DATASET_FORMAT_DESCRIPTIONS[self.name]


FOLDER_OF_LABELS_WILD = 'folder_of_labels_wild'
FOLDER_OF_LABELS_CROPPED = 'folder_of_labels_cropped'
FOLDER_OF_LABELS_CSV = 'folder_of_labels_csv'
FOLDER_OF_LABELS_CROPPED_BLACK = 'folder_of_labels_cropped_black'
ENCODING_128_CSV = 'face_encodings'
ENCODING_128_CSV_1_OUTPUT = 'face_encodings_with_1_output'
FOLDER_OF_LABELS_CSV_BLACK = 'folder_of_labels_csv_black'

DATASET_FORMAT_DESCRIPTIONS = {
    FOLDER_OF_LABELS_CROPPED: 'Dataset is presented by cropped images in label folders',
    FOLDER_OF_LABELS_CROPPED_BLACK: 'Cropped images in grayscale format',
    FOLDER_OF_LABELS_WILD: 'Dataset is presented by wild images in label folders',
    FOLDER_OF_LABELS_CSV: 'CSV created from images in label folders',
    FOLDER_OF_LABELS_CSV_BLACK: 'CSV created from grayscale images in label folders',
    ENCODING_128_CSV: 'Face encodings (128 numbers)',
    ENCODING_128_CSV_1_OUTPUT: 'Face encodings (128 numbers with 1 output'
}
