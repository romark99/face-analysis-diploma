from domain.model.repository.Repository import Repository


class NetWeightsRepository(Repository):

    def __init__(self) -> None:
        super().__init__()
        super().refreshEntities()

    def getEntity(self, specification):
        entity = super().getEntity(specification)
        self.determineModels(entity)
        return entity

    def determineModels(self, entity):
        raise NotImplementedError("'determineModels()' method should be overridden.")
