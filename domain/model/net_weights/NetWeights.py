class NetWeights:
    def __init__(self, type=None, name=None) -> None:
        self.type = type
        if name is None:
            self.date = None
            self.datasetName = None
            self.datasetFormatName = None
            self.netArchitectureName = None
        else:
            self.name = name
            self.fromNameToParams(name)
        self.models = []

    def fromNameToParams(self, name):
        strings = name.split('-')
        try:
            self.date = strings[0]
            self.datasetName = strings[1]
            self.datasetFormatName = strings[2]
            self.netArchitectureName = strings[3]
        except IndexError:
            self.date = None
            self.datasetName = None
            self.datasetFormatName = None
            self.netArchitectureName = None

    def getName(self):
        return self.name
        # return f'{self.date}-{self.datasetName}-{self.datasetFormatName}-{self.netArchitectureName}'
