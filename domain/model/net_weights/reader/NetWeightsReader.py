from domain.model.net_weights.NetWeights import NetWeights
from domain.model.reader.Reader import Reader


class NetWeightsReader(Reader):
    def __init__(self, netWeightsRepo):
        self.netWeightsRepo = netWeightsRepo

    def readNetWeightsName(self, netWeightsList):
        raise NotImplementedError("'readNetWeights()' method should be overridden.")

    def read(self, datasetType):
        specification = NetWeights(type=datasetType)
        netWeightsList = self.netWeightsRepo.getEntities(specification)
        netWeightsNames = [entity.getName() for entity in netWeightsList]

        netWeightsName = self.readNetWeightsName(netWeightsNames)
        if netWeightsName is None:
            return None

        specification.name = netWeightsName

        netWeights = self.netWeightsRepo.getEntity(specification)
        return netWeights
