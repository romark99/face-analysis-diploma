from keras.layers import Dense

from domain.model.net_layer.NetLayer import NetLayer


class DenseNetLayer(NetLayer):
    def __init__(self) -> None:
        self.units = None
        self.activation = 'linear'
        super().__init__()

    def getKerasLayer(self):
        kerasLayer = Dense(
            units=self.units,
            activation=self.activation
        )
        return kerasLayer

    @staticmethod
    def validateUnits(value):
        validationErrorMessage = f'"Units" field value is not valid. Allowed value: positive integer.'
        try:
            value = int(value)
            if value <= 0:
                return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage
