from domain.model.net_layer.dense.DenseNetLayer import DenseNetLayer
from domain.model.reader.Reader import Reader


class DenseNetLayerReader(Reader):
    def __init__(self):
        super().__init__()
        self.entity = DenseNetLayer()

    def readUnits(self):
        raise NotImplementedError("'readUnits()' method should be overridden.")

    def readActivation(self):
        raise NotImplementedError("'readActivation()' method should be overridden.")

    def read(self):
        entity = DenseNetLayer()
        entity.units = self.readUnits()
        entity.activation = self.readActivation()
        return entity
