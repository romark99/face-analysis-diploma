from keras.engine import InputLayer

from domain.model.net_layer.NetLayer import NetLayer


class InputNetLayer(NetLayer):
    def __init__(self):
        self.inputShape = None
        super().__init__()

    def getKerasLayer(self):
        kerasLayer = InputLayer(self.inputShape)
        return kerasLayer

    @staticmethod
    def validateInputShape(value):
        validationErrorMessage = f'"InputShape" field value is not valid. Allowed value: tuple of 1 to 3 integers.'
        try:
            value = list(int(x.strip()) for x in value.split(','))
            value = tuple(value)
            if len(value) < 1 or len(value) > 3:
                return validationErrorMessage
            for i in value:
                if i <= 0:
                    return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage
