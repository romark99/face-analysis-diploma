from domain.model.net_layer.input.InputNetLayer import InputNetLayer
from domain.model.reader.Reader import Reader


class InputNetLayerReader(Reader):

    def readInputShape(self):
        raise NotImplementedError("'readInputShape()' method should be overridden.")

    def read(self):
        entity = InputNetLayer()
        entity.inputShape = self.readInputShape()
        return entity
