from domain.model.net_layer.pooling.PoolingNetLayer import PoolingNetLayer
from domain.model.reader.Reader import Reader


class PoolingNetLayerReader(Reader):
    def readPoolSize(self):
        raise NotImplementedError("'readInputShape()' method should be overridden.")

    def readPadding(self):
        raise NotImplementedError("'readPadding()' method should be overridden.")

    def read(self):
        entity = PoolingNetLayer()
        entity.poolSize = self.readPoolSize()
        entity.padding = self.readPadding()
        return entity
