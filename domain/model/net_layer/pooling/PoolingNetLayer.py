from keras.layers import MaxPool2D

from domain.model.net_layer.NetLayer import NetLayer


class PoolingNetLayer(NetLayer):
    def __init__(self):
        self.poolSize = None
        self.padding = 'valid'
        super().__init__()

    def getKerasLayer(self):
        kerasLayer = MaxPool2D(
            pool_size=self.poolSize,
            padding=self.padding
        )
        return kerasLayer

    @staticmethod
    def validatePoolSize(value):
        validationErrorMessage = f'"PoolSize" field value is not valid. Allowed value: integer or tuple of 2 integers.'
        try:
            value = tuple(int(x.strip()) for x in value.split(','))
            if len(value) == 0 or len(value) > 2:
                return None, validationErrorMessage
            for i in value:
                if i < 0:
                    return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage
