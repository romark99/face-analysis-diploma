from domain.model.net_layer.flatten.FlattenNetLayer import FlattenNetLayer
from domain.model.reader.Reader import Reader


class FlattenNetLayerReader(Reader):
    def read(self):
        return FlattenNetLayer()
