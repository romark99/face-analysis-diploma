from keras.layers import Flatten

from domain.model.net_layer.NetLayer import NetLayer


class FlattenNetLayer(NetLayer):
    def getKerasLayer(self):
        kerasLayer = Flatten()
        return kerasLayer