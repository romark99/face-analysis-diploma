from keras.layers import BatchNormalization

from domain.model.net_layer.NetLayer import NetLayer


class BatchNormalizationNetLayer(NetLayer):
    def __init__(self) -> None:
        self.axis = -1
        super().__init__()

    def getKerasLayer(self):
        kerasLayer = BatchNormalization(axis=self.axis)
        return kerasLayer

    @staticmethod
    def validateAxis(value):
        validationErrorMessage = f'"Axis" field value is not valid. Allowed value: integer from -1 to 2.'
        try:
            if value == "":
                return None
            value = int(value)
            if value < -1 or value > 2:
                return (None, validationErrorMessage)
            return (value, None)
        except ValueError:
            return (None, validationErrorMessage)
