from domain.model.net_layer.batch_normalization.BatchNormalizationNetLayer import BatchNormalizationNetLayer
from domain.model.reader.Reader import Reader


class BatchNormalizationNetLayerReader(Reader):
    def readAxis(self):
        raise NotImplementedError("'readAxis()' method should be overridden.")

    def read(self):
        entity = BatchNormalizationNetLayer()
        axis = self.readAxis()
        if axis != "":
            entity.axis = axis
        return entity
