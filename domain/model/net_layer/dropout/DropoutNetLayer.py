from keras.layers import Dropout

from domain.model.net_layer.NetLayer import NetLayer


class DropoutNetLayer(NetLayer):
    def __init__(self):
        self.rate = None
        super().__init__()

    def getKerasLayer(self):
        kerasLayer = Dropout(
            rate=self.rate
        )
        return kerasLayer

    @staticmethod
    def validateRate(value):
        validationErrorMessage = f'"Rate" field value is not valid. Allowed value: float number between 0 and 1.'
        try:
            value = float(value)
            if value <= 0 or value >= 1:
                return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage
