from domain.model.net_layer.dropout.DropoutNetLayer import DropoutNetLayer
from domain.model.reader.Reader import Reader


class DropoutNetLayerReader(Reader):
    def readRate(self):
        raise NotImplementedError("'readRate()' method should be overridden.")

    def read(self):
        entity = DropoutNetLayer()
        entity.rate = self.readRate()
        return entity
