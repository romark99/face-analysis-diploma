class NetLayer:
    CONVOLUTIONAL = 'CONVOLUTIONAL'
    DENSE = 'DENSE'
    ACTIVATION = 'ACTIVATION'
    DROPOUT = 'DROPOUT'
    FLATTEN = 'FLATTEN'
    BATCH_NORMALIZATION = 'BATCH_NORMALIZATION'
    POOLING = 'POOLING'

    TYPES = (
        ACTIVATION,
        BATCH_NORMALIZATION,
        CONVOLUTIONAL,
        DENSE,
        DROPOUT,
        FLATTEN,
        POOLING
    )

    def getKerasLayer(self):
        raise NotImplementedError("'getKerasLayer()' method should be overridden.")
