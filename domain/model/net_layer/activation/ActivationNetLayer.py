from keras.layers import Activation

from domain.model.net_layer.NetLayer import NetLayer


class ActivationNetLayer(NetLayer):
    def __init__(self) -> None:
        self.activation = 'linear'
        super().__init__()

    def getKerasLayer(self):
        kerasLayer = Activation(
            activation=self.activation
        )
        return kerasLayer
