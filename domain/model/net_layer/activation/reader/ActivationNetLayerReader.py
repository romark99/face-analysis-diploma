from domain.model.net_layer.activation.ActivationNetLayer import ActivationNetLayer
from domain.model.reader.Reader import Reader


class ActivationNetLayerReader(Reader):
    def readActivation(self):
        raise NotImplementedError("'readActivation()' method should be overridden.")

    def read(self):
        entity = ActivationNetLayer()
        entity.activation = self.readActivation()
        return entity
