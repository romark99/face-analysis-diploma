from domain.model.net_layer.convolutional.ConvolutionalNetLayer import ConvolutionalNetLayer
from domain.model.reader.Reader import Reader


class ConvolutionalNetLayerReader(Reader):
    def readFilters(self):
        raise NotImplementedError("'readFilters()' method should be overridden.")

    def readKernelSize(self):
        raise NotImplementedError("'readKernelSize()' method should be overridden.")

    def readActivation(self):
        raise NotImplementedError("'readActivation()' method should be overridden.")

    def readPadding(self):
        raise NotImplementedError("'readPadding()' method should be overridden.")

    def read(self):
        entity = ConvolutionalNetLayer()
        entity.filters = self.readFilters()
        entity.kernelSize = self.readKernelSize()
        entity.activation = self.readActivation()
        entity.padding = self.readPadding()
        return entity
