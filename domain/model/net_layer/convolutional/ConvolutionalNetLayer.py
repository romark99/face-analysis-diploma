from keras.layers import Conv2D

from domain.model.net_layer.NetLayer import NetLayer


class ConvolutionalNetLayer(NetLayer):

    def __init__(self) -> None:
        self.filters = None
        self.kernelSize = None
        self.activation = 'linear'
        self.padding = 'valid'
        super().__init__()

    def getKerasLayer(self):
        kerasLayer = Conv2D(
            filters=self.filters,
            kernel_size=self.kernelSize,
            activation=self.activation,
            padding=self.padding
        )
        return kerasLayer

    @staticmethod
    def validateFilters(value):
        validationErrorMessage = f'"Filters" field value is not valid. Allowed value: positive integer.'
        try:
            value = int(value)
            if value <= 0:
                return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage

    @staticmethod
    def validateKernelSize(value):
        validationErrorMessage = f'"KernelSize" field value is not valid. Allowed value: integer or tuple of 2 integers.'
        try:
            value = tuple(int(x.strip()) for x in value.split(','))
            if len(value) == 0 or len(value) > 2:
                return None, validationErrorMessage
            for i in value:
                if i < 0:
                    return None, validationErrorMessage
            return value, None
        except ValueError:
            return None, validationErrorMessage
