from keras import Sequential


class NetArchitecture:
    def __init__(self, type=None, name=None) -> None:
        self.type = type
        self.name = name
        self.kerasArchitecture = None
        self.inputShape = None
        self.outputShape = None
        super().__init__()

    def createKerasArchitecture(self):
        self.kerasArchitecture = Sequential(name=self.name)

    def addLayer(self, layer):
        self.kerasArchitecture.add(layer)

    def getKerasArchitecture(self):
        return self.kerasArchitecture

    def determineShapes(self):
        self.inputShape = self.kerasArchitecture.layers[0].input_shape[1:]
        self.outputShape = self.kerasArchitecture.layers[-1].output_shape[1:]

    def getName(self):
        return self.name

    def getSummary(self):
        return self.kerasArchitecture.summary()

    def build(self, netParams):
        self.kerasArchitecture.compile(loss=netParams.loss,
                                       optimizer=netParams.optimizer,
                                       metrics=['accuracy'])

    def predictTestData(self, testDatum):
        return self.kerasArchitecture.predict(testDatum)[0]

    @staticmethod
    def validateName(value):
        validationErrorMessage = f'"Name" field value is not valid. Allowed value: non-empty string.'
        try:
            if value == "" or value.isspace():
                return (None, validationErrorMessage)
            return (value, None)
        except ValueError:
            return (None, validationErrorMessage)
