from domain.model.net_architecture.NetArchitecture import NetArchitecture
from domain.model.reader.Reader import Reader


class NetArchitectureReader(Reader):
    def readType(self):
        raise NotImplementedError("'readType()' method should be overridden.")

    def readName(self):
        raise NotImplementedError("'readName()' method should be overridden.")

    def readInputLayer(self):
        raise NotImplementedError("'readInputLayer()' method should be overridden.")

    def readLayer(self, isWithInput):
        raise NotImplementedError("'readLayers()' method should be overridden.")

    def displayNetArchitectureSummary(self, summary):
        raise NotImplementedError("'displayNetArchitecture()' method should be overridden.")

    def read(self, datasetType=None):
        entity = NetArchitecture()

        if datasetType is None:
            entity.type = self.readType()
        else:
            entity.type = datasetType

        entity.name = self.readName()
        entity.createKerasArchitecture()

        inputLayer = self.readInputLayer()
        kerasInputLayer = inputLayer.getKerasLayer()
        entity.addLayer(kerasInputLayer)

        summary = entity.getSummary()
        self.displayNetArchitectureSummary(summary)

        isWithInput = True

        while True:
            layer = self.readLayer(isWithInput)

            if not layer:
                entity.determineShapes()
                return entity

            kerasLayer = layer.getKerasLayer()
            entity.addLayer(kerasLayer)

            isWithInput = False

            summary = entity.getSummary()
            self.displayNetArchitectureSummary(summary)
