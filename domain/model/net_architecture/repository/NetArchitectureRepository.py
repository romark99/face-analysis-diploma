from domain.model.repository.Repository import Repository


class NetArchitectureRepository(Repository):

    def __init__(self) -> None:
        super().__init__()
        super().refreshEntities()

    def determineKerasArchitecture(self, entity):
        raise NotImplementedError("'determineKerasArchitecture()' method should be overridden.")

    def getEntity(self, specification):
        entity = super().getEntity(specification)
        self.determineKerasArchitecture(entity)
        entity.determineShapes()
        return entity
