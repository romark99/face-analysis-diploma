import os

IGNORE_ERRORS = True

IS_USE_AGE_WITH_1_OUTPUT = False

# Activations
ACTIVATIONS = [
    'linear',
    'relu',
    'sigmoid',
    'tanh',
    'softmax'
]

DEFAULT_ACTIVATION = 'linear'

PADDINGS = [
    'valid',
    'same'
]

DEFAULT_PADDING = 'valid'

LOSSES = [
    'mean_squared_error',
    'binary_crossentropy',
    'categorical_crossentropy'
]

MONITORS = [
    'val_loss',
    'val_accuracy',
    'loss',
    'accuracy'
]

SGD = 'SGD'
RMS_PROP = 'RMSProp'
ADAM = 'Adam'

OPTIMIZERS = [
    SGD,
    RMS_PROP,
    ADAM
]

DEFAULT_ALPHAS = {
    SGD: 0.01,
    RMS_PROP: 0.001,
    ADAM: 0.001
}

DEFAULT_LOSS = 'mean_squared_error'
DEFAULT_MONITOR = 'val_loss'
DEFAULT_OPTIMIZER = 'SGD'

DATASET_TYPES = [
    'age',
    'emotion',
    'face',
    'gender'
]

TOLERANCE = 0.6

# Directories
ROOT_DIR_FULL_PATH = os.path.dirname(os.path.abspath(__file__))
DIR_DATA_NAME = 'data'
DIR_DATA_FULL_PATH = os.path.join(ROOT_DIR_FULL_PATH, DIR_DATA_NAME)

DIR_ARCHITECTURE_NAME = 'architectures'
DIR_CONFIGURATION_NAME = 'configurations'
DIR_DATASET_NAME = 'datasets'
DIR_MODEL_NAME = 'models'

# Files
FILE_ACCURACY_PNG = 'accuracy.png'
FILE_DATASET_PARAMS_INFO = 'dataset_params_info.json'
FILE_ERROR_MATRIX_TXT = 'error_matrix.txt'
FILE_LOSS_PNG = 'loss.png'
FILE_RESULTS_TXT = 'results.txt'

# String literals
CHOOSE_DATASET = 'Choose dataset:'
CHOOSE_DATASET_FOLDER = 'Choose dataset folder:'
CHOOSE_DATASET_FORMAT = 'Choose dataset format:'
CHOOSE_DATASET_TYPE = 'Choose dataset type:'
CHOOSE_NET_LAYER_TYPE = 'Choose layer type:'
COPYING_DATASET_FOLDER = 'Copying a dataset folder...'
DATASET_FOLDER_IS_NOT_CHOSEN = "You didn't choose a folder."
DATASET_FOLDER_IS_NOT_FOUND = 'A dataset folder is not found.'
DATASET_SUCCESSFULLY_ADDED = 'The dataset was successfully added.'
DATASET_TYPE_FOLDER_IS_NOT_FOUND = 'A dataset type folder is not found.'
DATASET_WITH_SUCH_FORMAT_ALREADY_EXISTS = 'A dataset of this format already exists.'
DATASETS_OF_FOLLOWING_TYPE_NOT_FOUND = 'Datasets of the following type were not found.'
ENTER_ANY_STRING_TO_CONTINUE = 'Enter any string to continue...'
ERROR_WHEN_COPYING_DATASET = 'Something got wrong during copying a dataset folder.'
FEATURE_IS_NOT_IMPLEMENTED_YET = 'Sorry, but this feature is not implemented yet.'
STAR_STRING = '******************************************'
SOMETHING_GOT_WRONG = 'Something has got wrong. :('
WRONG_OPTION_MESSAGE = 'You have chosen wrong option. Please, try again...'
