import csv
import os

import cv2
import face_recognition
import numpy as np

# Get a reference to webcam #0 (the default one)
from definitions import DIR_DATA_FULL_PATH, DIR_DATASET_NAME, IS_USE_AGE_WITH_1_OUTPUT
from domain.model.dataset_format import DatasetFormat
# This is a demo of running face recognition on live video from your webcam. It's a little more complicated than the
# other example, but it includes some basic performance tweaks to make things run a lot faster:
#   1. Process each video frame at 1/4 resolution (though still display it at full resolution)
#   2. Only detect faces in every other frame of video.
# PLEASE NOTE: This example requires OpenCV (the `cv2` library) to be installed only to read from your webcam.
# OpenCV is *not* required to use the face_recognition library. It's only required if you want to run this
# specific demo. If you have trouble installing it, try any of the other demos that don't require it instead.
from domain.model.net_architecture.NetArchitecture import NetArchitecture
from infrastructure.domain.model.data_reader.folder_with_labels_csv_black.console.FolderWithLabelsCsvBlackDataReaderImpl import \
    FolderWithLabelsCsvBlackDataReaderImpl
from infrastructure.domain.model.repository.net_architecture.json.NetArchitectureRepositoryJSONImpl import \
    NetArchitectureRepositoryJSONImpl
from infrastructure.domain.model.utils.cropping.CroppingUtils import CroppingUtils
from infrastructure.domain.model.utils.prediction import PredictionUtils
from infrastructure.domain.model.utils.prediction.PredictionUtils import getSemanticPrediction

video_capture = cv2.VideoCapture(0)

# FACE constants
# FACES_CSV_PATH = os.path.join(DIR_DATA_FULL_PATH, 'face', DIR_DATASET_NAME, 'home_dataset',
#                               DatasetFormat.ENCODING_128_CSV, f'{DatasetFormat.ENCODING_128_CSV}.csv')

FACES_CSV_PATH = os.path.join(DIR_DATA_FULL_PATH, 'face', DIR_DATASET_NAME, 'diploma_comission_dataset',
                              DatasetFormat.ENCODING_128_CSV, f'{DatasetFormat.ENCODING_128_CSV}.csv')

# GENDER constants
GENDER_MODEL_PATH = os.path.join(DIR_DATA_FULL_PATH, 'gender', 'models',
                                 '20200517_013413-UTKFace-face_encodings-G_FC_Encoding_4',
                                 'model.best_loss-016-0.16-0.9384.h5')
AGE_MODEL_PATH_WITH_1_OUTPUT = os.path.join(DIR_DATA_FULL_PATH, 'age', 'models',
                                            '20200609_234436-UTK_Face-face_encodings_with_1_output-A_FC_Encoding_1_output',
                                            'model.best_loss-027-0.00-0.0495.h5')
AGE_MODEL_PATH = os.path.join(DIR_DATA_FULL_PATH, 'age', 'models',
                              '20200610_011225-UTK_Face-face_encodings-A_FC_Encoding',
                              'model.best_loss-094-2.95-0.1757.h5')
EMOTION_MODEL_PATH = os.path.join(DIR_DATA_FULL_PATH, 'emotion', 'models',
                                  '20200601_010823-ME_7-folder_of_labels_csv_black-E_Xception',
                                  'model.best_loss-010-1.11-0.6207.h5')

# FRAME constants
KOEFF_DECREASE = 2
MARGIN = 30

ages = []


def getMiddleAge(age):
    global ages
    ages.append(int(age))
    if len(ages) > 20:
        ages = ages[-20:]
    mean = np.mean(ages)
    return int(round(mean))


utils = CroppingUtils()

repo = NetArchitectureRepositoryJSONImpl()


def getAgeModel():
    if IS_USE_AGE_WITH_1_OUTPUT:
        specification = NetArchitecture('age', 'A_FC_Encoding_1_output')
    else:
        specification = NetArchitecture('age', 'A_FC_Encoding')
    netArchitecture = repo.getEntity(specification)
    kerasModel = netArchitecture.getKerasArchitecture()
    if IS_USE_AGE_WITH_1_OUTPUT:
        kerasModel.load_weights(AGE_MODEL_PATH_WITH_1_OUTPUT)
    else:
        kerasModel.load_weights(AGE_MODEL_PATH)
    return kerasModel


def getEmotionModel():
    specification = NetArchitecture('emotion', 'E_Xception')
    netArchitecture = repo.getEntity(specification)
    kerasModel = netArchitecture.getKerasArchitecture()
    kerasModel.load_weights(EMOTION_MODEL_PATH)
    # kerasModel.summary()
    # exit(0)
    return kerasModel


def getGenderModel():
    specification = NetArchitecture('gender', 'G_FC_Encoding_4')
    netArchitecture = repo.getEntity(specification)
    kerasModel = netArchitecture.getKerasArchitecture()
    kerasModel.load_weights(GENDER_MODEL_PATH)
    return kerasModel


def getFaceEncodings():
    encodings = []
    encodings_label = []
    img_names = []
    with open(FACES_CSV_PATH) as f:
        csv_reader = csv.reader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            img_name = row.pop(0)
            label = row.pop(0)
            encodings.append(row)
            encodings_label.append(label)
            img_names.append(img_name)
            line_count += 1
        print(f'ALL: Processed {line_count} lines.')
    encodings = np.array(encodings, 'float32')
    return (encodings, encodings_label, img_names)


def getPersonPrediction(face_encoding):
    matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
    name = "Unknown"

    face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
    best_match_index = np.argmin(face_distances)
    if matches[best_match_index]:
        name = known_face_names[best_match_index]
    return name


def getGenderPrediction(face_encoding):
    face_encoding = np.reshape(face_encoding, (1, -1))
    yPredicted = GENDER_MODEL.predict(face_encoding)
    return getSemanticPrediction('gender', yPredicted)


def getAgePrediction(face_encoding):
    face_encoding = np.reshape(face_encoding, (1, -1))
    yPredicted = AGE_MODEL.predict(face_encoding)
    if not IS_USE_AGE_WITH_1_OUTPUT:
        ages = np.arange(0, 116).reshape(116, 1)
        yPredicted = yPredicted.dot(ages).flatten()
    return getSemanticPrediction('age', yPredicted[0])


def getEmotionPrediction(croppedNotScaledFrame):
    imageReader = FolderWithLabelsCsvBlackDataReaderImpl()
    x_test = imageReader.readFromCroppedNotScaledImageArray(croppedNotScaledFrame,
                                                            EMOTION_MODEL.layers[0].input_shape[1:])
    yPredicted = EMOTION_MODEL.predict(x_test)
    return getSemanticPrediction('emotion', yPredicted)


def getCommonPrediction(croppedNotScaledFrame, face_encoding):
    personName = getPersonPrediction(face_encoding)
    gender = getGenderPrediction(face_encoding)
    age = getAgePrediction(face_encoding)
    # age = getMiddleAge(age)
    emotion = getEmotionPrediction(croppedNotScaledFrame)

    return personName, gender, age, emotion


X_Encodings, Y, IMG = getFaceEncodings()

# Create arrays of known face encodings and their names
known_face_encodings = X_Encodings
known_face_names = Y
known_image_names = IMG

GENDER_MODEL = getGenderModel()
AGE_MODEL = getAgeModel()
EMOTION_MODEL = getEmotionModel()

# Initialize some variables
face_locations = []
face_encodings = []
predictions = []
process_this_frame = True

while True:
    # Grab a single frame of video
    ret, frame = video_capture.read()

    # Resize frame of video to 1/2 size for faster face recognition processing
    small_frame = cv2.resize(frame, (0, 0), fx=1 / KOEFF_DECREASE, fy=1 / KOEFF_DECREASE)

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_small_frame = small_frame[:, :, ::-1]

    # Only process every other frame of video to save time
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        predictions = []
        for i in range(len(face_encodings)):
            top, right, bottom, left = face_location = face_locations[i]
            face_encoding = face_encodings[i]

            croppedNotScaledFrame = rgb_small_frame[top:(bottom + 1), left:(right + 1), :]
            prediction = getCommonPrediction(croppedNotScaledFrame, face_encoding)

            # See if the face is a match for the known face(s)
            predictions.append(prediction)

    process_this_frame = not process_this_frame

    # Display the results
    for (top, right, bottom, left), prediction in zip(face_locations, predictions):
        # Scale back up face locations since the frame we detected in was scaled to 1/2 size
        top *= KOEFF_DECREASE
        right *= KOEFF_DECREASE
        bottom *= KOEFF_DECREASE
        left *= KOEFF_DECREASE

        top -= MARGIN * 2
        left -= MARGIN
        right += MARGIN
        bottom += MARGIN

        personName, gender, age, emotion = prediction
        ageCategory = PredictionUtils.getAgeCategory(age, gender)

        # Draw a box around the face
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # Draw a label with a name below the face
        # cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        cv2.rectangle(frame, (left, bottom - 40), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, f'{age}, {ageCategory}', (left + 6, bottom - 6), font, 0.6,
                    (255, 255, 255), 1)
        cv2.putText(frame, f'{personName}, {gender}, {emotion}', (left + 6, bottom - 26), font, 0.6,
                    (255, 255, 255), 1)

    # Display the resulting image
    cv2.imshow('Video', frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release handle to the webcam
video_capture.release()
cv2.destroyAllWindows()
