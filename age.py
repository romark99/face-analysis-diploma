import numpy as np

from infrastructure.domain.model.utils.prediction.PredictionUtils import getAgeIndex


def getAgeLabels(dataset, numberOfClasses, interval):
    labels = np.zeros((dataset.shape[0], numberOfClasses + interval - 1))
    f = 0
    for i_plus1 in dataset:
        i = int(i_plus1 - 1)
        for j in range(interval):
            inx = i + j
            labels[f, inx] = 1
        f += 1
    return labels


def determineGenderByImageName(imageName):
    a = imageName.split('_')[1]
    if a == '0':
        return True
    return False


def getAgeLonelyLabels(dataset, numberOfClasses, interval):
    labels = np.zeros((dataset.shape[0], numberOfClasses // interval + 1))
    f = 0
    for i_plus1 in dataset:
        i = int(i_plus1 - 1)
        group = i // interval
        labels[f, group] = 1
        f += 1
    return labels

from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.callbacks import EarlyStopping
import csv

# 8337
from keras.optimizers import RMSprop
from sklearn.model_selection import train_test_split

csv_file = "./datasets/age/UTK_Face/face_encodings/face_encodings.csv"

encodings = []
encodings_label = []
img_names = []

test_encodings = []
test_encodings_label = []

batch_size = 64

epochs = 5000


def read_encodings(encodings, encodings_label, img_names):
    with open(csv_file) as f:
        csv_reader = csv.reader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            img_name = row.pop(0)
            label = int(row.pop(0))
            encodings.append(row)
            encodings_label.append(label)
            img_names.append(img_name)
            line_count += 1
        print(f'ALL: Processed {line_count} lines.')

    return (encodings, encodings_label, img_names)


X, Y, IMG = read_encodings(encodings, encodings_label, img_names)

# x_train, x_test, y_train, y_test, img_names_train, img_names_test = \
#     train_test_split(X, Y, IMG, test_size=0.1, random_state=4)

x_train, x_test, y_train, y_test, img_names_train, img_names_test = \
    train_test_split(X, Y, IMG, test_size=0.1)

print(f'Training: Processed {len(x_train)} lines.')
print(f'Testing: Processed {len(x_test)} lines.')

x_train = np.array(x_train, 'float32')
y_train = np.array(y_train, 'float32')
x_test = np.array(x_test, 'float32')
y_test = np.array(y_test, 'float32')

# y_train=np_utils.to_categorical(y_train, num_classes=num_labels)
# y_test=np_utils.to_categorical(y_test, num_classes=num_labels)

# y_train = getAgeLonelyLabels(y_train, 116, 10)
# y_test = getAgeLonelyLabels(y_test, 116, 10)

y_train = ((y_train - 1) / 116).reshape(-1, 1)
y_test = ((y_test - 1) / 116).reshape(-1, 1)

num_labels = y_train.shape[1]

model = Sequential([
    Dense(256, input_shape=(128,)),
    Activation('relu'),
    Dropout(0.2),
    Dense(256),
    Activation('relu'),
    Dropout(0.2),
    Dense(256),
    Activation('relu'),
    Dropout(0.2),
    Dense(128),
    Activation('relu'),
    Dropout(0.2),
    Dense(16),
    Activation('relu'),
    Dropout(0.2),
    Dense(1),
    Activation('sigmoid')
])

optimizer = RMSprop(lr=0.001)
model.compile(loss='mean_squared_error',
              optimizer=optimizer,
              metrics=['accuracy'])

from keras.utils import plot_model

plot_model(model, to_file='model_new.png', show_shapes=True)

early_stopping = EarlyStopping(monitor='val_loss', patience=10, min_delta=0.00005)
# mcp_save = ModelCheckpoint('model.' + dataset + '-{epoch:02d}-{val_loss:.2f}-{val_accuracy:.4f}.hdf5', save_best_only=True, monitor='val_loss', mode='min')

H = model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test),
              shuffle=True,
              callbacks=[early_stopping])

y_predicted = model.predict(x_test, batch_size=batch_size, verbose=1)

ageCategoriesMatrix = np.zeros((10, 10))

sum1 = 0
for i in range(y_test.shape[0]):
    print('-------')
    print(img_names_test[i])
    testAge = int(round(y_test[i][0] * 116 + 1))
    predictedAge = int(round(y_predicted[i][0] * 116 + 1))
    print(f'TST AGE: {testAge}')
    print(f'PRD AGE: {predictedAge}')
    print(f'DIFF: {testAge - predictedAge}')
    print('-------')
    gender = determineGenderByImageName(img_names_test[i])
    pred_label = getAgeIndex(predictedAge, gender)
    et_label = getAgeIndex(testAge, gender)
    ageCategoriesMatrix[pred_label][et_label] += 1
    if pred_label == et_label:
        sum1 += 1
print(100 * sum1 / len(y_predicted))
print("Matrix:")
for i in range(10):
    print(ageCategoriesMatrix[i])

# model.save("mymodel_new.model")
#
# # plot training/validation loss/accuracy
# plt.style.use("ggplot")
# plt.figure()
# N = len(H.history["loss"])
# plt.plot(np.arange(0,N), H.history["loss"], label="train_loss")
# plt.plot(np.arange(0,N), H.history["val_loss"], label="val_loss")
# # plt.plot(np.arange(0,N), H.history["accuracy"], label="train_accuracy")
# # plt.plot(np.arange(0,N), H.history["val_accuracy"], label="val_accuracy")
#
# #LearningPolicy
# #LearningRateSheduler step_decay
#
# plt.title("Training Loss and Accuracy")
# plt.xlabel("Epoch #")
# plt.ylabel("Loss/Accuracy")
# plt.legend(loc="upper right")
#
# # save plot to disk
# plt.savefig("loss.png")
#
# y_predicted = model.predict(x_test, batch_size=batch_size)
#
# mtx = np.zeros( (num_labels,num_labels))
#
# sum1 = 0
# for i in range(len(y_predicted)):
#     pred_label = np.argmax(y_predicted[i])
#     et_label = np.argmax(y_test[i])
#     mtx[pred_label][et_label] += 1
#     if pred_label == et_label:
#         sum1 += 1
# print(100 * sum1/len(y_predicted))
# print("Matrix:")
# for i in range(num_labels):
#     print(mtx[i])
