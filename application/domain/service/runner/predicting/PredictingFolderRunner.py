import os

from domain.model.dataset_format.DatasetFormat import DatasetFormat
from domain.model.runner.Runner import Runner


class PredictingFolderRunner(Runner):
    def __init__(self, loadNetArchitectureWithWeightsRunner, getDataReaderByFormat):
        self.loadNetArchitectureWithWeightsRunner = loadNetArchitectureWithWeightsRunner
        self.getDataReaderByFormat = getDataReaderByFormat

    def readDatasetType(self):
        raise NotImplementedError("'readDatasetType()' method should be overridden.")

    def readDatasetFormatName(self):
        raise NotImplementedError("'readDatasetFormatName()' method should be overridden.")

    def readInputFolder(self):
        raise NotImplementedError("'readInputFolder()' method should be overridden.")

    def displayImageNotDetected(self):
        raise NotImplementedError("'displayImageNotDetected()' method should be overridden.")

    def displayImageName(self, imageName):
        raise NotImplementedError("'displayPredictions()' method should be overridden.")

    def displayPrediction(self, datasetType, predictions):
        raise NotImplementedError("'displayPrediction()' method should be overridden.")

    def displayFolderIsNotChosen(self):
        raise NotImplementedError("'displayPrediction()' method should be overridden.")

    def run(self):
        datasetType = self.readDatasetType()

        self.loadNetArchitectureWithWeightsRunner.setDatasetType(datasetType)
        self.loadNetArchitectureWithWeightsRunner.run()
        netArchitecture = self.loadNetArchitectureWithWeightsRunner.getNetArchitecture()
        netWeights = self.loadNetArchitectureWithWeightsRunner.getNetWeights()
        if netWeights is None:
            return

        datasetFormat = DatasetFormat()
        datasetFormat.name = netWeights.datasetFormatName

        if datasetFormat.name == None:
            datasetFormat.name = self.readDatasetFormatName()

        self.getDataReaderByFormat.execute(datasetFormat)

        testDataReader = self.getDataReaderByFormat.getDataReader()

        inputFolder = self.readInputFolder()
        if inputFolder is None or not os.path.exists(inputFolder):
            self.displayFolderIsNotChosen()
            return

        imageNames = os.listdir(inputFolder)
        imageNames.sort()

        for imageName in imageNames:
            if imageName.endswith('.png') or imageName.endswith('.jpg') or imageName.endswith('.jpeg'):
                self.displayImageName(imageName)
                imagePath = os.path.join(inputFolder, imageName)
                preparedImage = testDataReader.readFromWildImagePath(imagePath, netArchitecture.inputShape)
                if preparedImage is None:
                    self.displayImageNotDetected()
                    continue
                prediction = netArchitecture.predictTestData(preparedImage)
                self.displayPrediction(datasetType, prediction)
