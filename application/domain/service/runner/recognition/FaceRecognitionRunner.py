import numpy as np
from face_recognition import face_distance

from definitions import TOLERANCE
from domain.model.dataset.Dataset import Dataset
from domain.model.dataset_format import DatasetFormat
from domain.model.runner.Runner import Runner


class FaceRecognitionRunner(Runner):

    def chooseUnknownDataset(self, datasetType):
        raise NotImplementedError("'isToUseSavedModel()' method should be overridden.")

    def getFaceData(self, dataset, unknownDatasetName):
        raise NotImplementedError("'getFaceData()' method should be overridden.")

    def readDatasetName(self, datasetType):
        raise NotImplementedError("'readDatasetName()' method should be overridden.")

    def determineDatasetFormat(self, dataset, datasetFormatName):
        raise NotImplementedError("'determineDatasetFormat()' method should be overridden.")

    def run(self, datasetType):
        dataset = Dataset()
        dataset.type = datasetType

        dataset.name = self.readDatasetName(datasetType)

        if dataset.name is None:
            return

        dataset = self.determineDatasetFormat(dataset, DatasetFormat.ENCODING_128_CSV)

        if dataset.format is None:
            return

        unknownDatasetName = self.chooseUnknownDataset(dataset.type)
        x_train, x_test, y_train, y_test, img_names_train, img_names_test = self.getFaceData(dataset,
                                                                                             unknownDatasetName)
        sum = 0
        for i in range(len(x_test)):
            print(f'IMG_NAME : {img_names_test[i]}')
            y_predicted, img_predicted, destination = self.predictFace(x_train, x_test[i], y_train, img_names_train)
            print(f'{img_predicted} : {destination}')
            print(y_test[i])
            if (y_predicted == y_test[i]):
                sum += 1
        acc = 100 * sum / len(x_test)
        print(f"Accuracy: {acc}")

    def predictFace(self, x_train, x_test_datum, y_train, img_names_train):
        encodings = face_distance(x_train, x_test_datum)
        destination = min(encodings)
        if destination > TOLERANCE:
            y_predicted = 'unknown'
            img_predicted = 'unknown'
        else:
            min_inx = np.argmin(encodings)
            img_predicted = img_names_train[min_inx]
            y_predicted = y_train[min_inx]
        return y_predicted, img_predicted, destination
