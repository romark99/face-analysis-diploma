import os
from datetime import datetime

from definitions import DIR_DATA_FULL_PATH, DIR_MODEL_NAME
from domain.model.runner.Runner import Runner


class TrainingRunner(Runner):
    def __init__(self,
                 chooseDatasetTypeExecutor,
                 determineNetArchitectureRunner,
                 loadNetArchitectureRunner,
                 netParamsReader,
                 determineDatasetRunner,
                 getDataReaderByDatasetFormatExecutor,
                 validateDatasetAndArchitectureExecutor,
                 trainKerasModelExecutor) -> None:
        self.chooseDatasetTypeExecutor = chooseDatasetTypeExecutor
        self.determineNetArchitectureRunner = determineNetArchitectureRunner
        self.loadNetArchitectureRunner = loadNetArchitectureRunner
        self.netParamsReader = netParamsReader
        self.determineDatasetRunner = determineDatasetRunner
        self.getDataReaderByDatasetFormatExecutor = getDataReaderByDatasetFormatExecutor
        self.validateDatasetAndArchitectureExecutor = validateDatasetAndArchitectureExecutor
        self.trainKerasModelExecutor = trainKerasModelExecutor
        super().__init__()

    def isToUseSavedModel(self):
        raise NotImplementedError("'isToUseSavedModel()' method should be overridden.")

    def processResults(self, datasetType, datasetFormatName, kerasHistory, saveFolderPath, totalTime, imageNames):
        raise NotImplementedError("'processResults()' method should be overridden.")

    def isUseFaceRecognitionModule(self):
        raise NotImplementedError("'isUseFaceRecognitionModule()' method should be overridden.")

    def runFaceRecognition(self, datasetType):
        raise NotImplementedError("'runFaceRecognition()' method should be overridden.")

    def run(self) -> None:
        self.chooseDatasetTypeExecutor.execute()
        datasetType = self.chooseDatasetTypeExecutor.getDatasetType()

        self.determineDatasetRunner.setDatasetType(datasetType)
        if datasetType == 'face' and self.isUseFaceRecognitionModule():
            self.runFaceRecognition(datasetType)
            return

        self.determineDatasetRunner.run()
        dataset = self.determineDatasetRunner.getDataset()
        if not dataset:
            return

        if self.isToUseSavedModel():
            self.loadNetArchitectureRunner.setDatasetType(datasetType)
            self.loadNetArchitectureRunner.run()
            netArchitecture = self.loadNetArchitectureRunner.getNetArchitecture()
        else:
            self.determineNetArchitectureRunner.setDatasetType(datasetType)
            self.determineNetArchitectureRunner.run()
            netArchitecture = self.determineNetArchitectureRunner.getNetArchitecture()

        if netArchitecture is None:
            return

        netParams = self.netParamsReader.read()
        if netParams is None:
            return

        self.getDataReaderByDatasetFormatExecutor.execute(dataset.format)
        dataReader = self.getDataReaderByDatasetFormatExecutor.getDataReader()
        if not dataReader:
            return

        netData, imageNames = dataReader.read(dataset.path, dataset.format.inputShape,
                                              dataset.format.numberOfClasses)

        self.validateDatasetAndArchitectureExecutor.execute(netArchitecture.getKerasArchitecture(), dataset.format)

        if not self.validateDatasetAndArchitectureExecutor.getIsValid():
            return

        netArchitecture.build(netParams)

        saveFolderPath = determineSaveFolder(datasetType, dataset.name, dataset.format.name,
                                             netArchitecture.name)

        self.trainKerasModelExecutor.execute(netArchitecture.getKerasArchitecture(), netData, netParams, saveFolderPath)
        kerasHistory = self.trainKerasModelExecutor.getKerasHistory()
        totalTime = self.trainKerasModelExecutor.getTotalTime()

        self.processResults(datasetType, dataset.format.name, kerasHistory, saveFolderPath, totalTime, imageNames)


def determineSaveFolder(datasetType, datasetName, datasetFormatName, configurationName):
    timeString = datetime.now().strftime("%Y%m%d_%H%M%S")

    folderName = f'{timeString}-{datasetName}-{datasetFormatName}-{configurationName}'
    modelPath = os.path.join(DIR_DATA_FULL_PATH, datasetType, DIR_MODEL_NAME, folderName)
    return modelPath
