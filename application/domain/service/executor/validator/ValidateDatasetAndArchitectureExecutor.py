from domain.model.executor.Executor import Executor


class ValidateDatasetAndArchitectureExecutor(Executor):

    def __init__(self) -> None:
        self.isValid = None
        super().__init__()

    def execute(self, kerasModel, datasetFormat):
        modelInputShape = kerasModel.layers[0].input_shape
        modelOutputShape = kerasModel.layers[-1].output_shape

        self.isValid = self.validateInputShape(modelInputShape, datasetFormat.inputShape) and self.validateOutputShape(
            modelOutputShape, datasetFormat.numberOfClasses)

    def validateInputShape(self, modelInputShape, datasetInputShape):
        if len(modelInputShape) - 1 != len(datasetInputShape):
            return False
        else:
            for i in range(len(datasetInputShape)):
                if datasetInputShape[i] is not None and datasetInputShape[i] != modelInputShape[i + 1]:
                    return False
        return True

    def validateOutputShape(self, modelOutputShape, datasetNumberOfClasses):
        if len(modelOutputShape) != 2:
            return False
        elif modelOutputShape[1] != datasetNumberOfClasses:
            return False
        return True

    def getIsValid(self):
        return self.isValid
