from domain.model.menu.Menu import Menu


class NetArchitecturesMenu(Menu):
    LIST_NET_ARCHITECTURES = 'Display all network architectures'
    CREATE_NET_ARCHITECTURE = 'Create a new network architecture'

    def __init__(self, title, choices):
        super().__init__(title, choices)
