from domain.model.menu.Menu import Menu


class MainMenu(Menu):
    RECOGNITION = 'Go to recognition'
    PROGRAM_INFO = 'Program info'
    DATASETS = 'Work with datasets'
    NETWORK_ARCHITECTURES = 'Work with network architectures'
    TRAINING = 'Training the model'
    PREDICTING_A_FOLDER_WITH_IMAGES = 'Predicting images in a folder'
    WEBCAM_RECOGNITION = 'Webcam recognition'

    def __init__(self, title, choices):
        super().__init__(title, choices)

