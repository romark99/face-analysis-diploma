from domain.model.menu.Menu import Menu


class DatasetsMenu(Menu):
    LIST_DATASETS = 'Display all datasets'
    ADD_DATASET = 'Add new dataset'
    REFORMAT_DATASET = 'Reformat dataset'

    def __init__(self, title, choices):
        super().__init__(title, choices)
