from domain.model.executor.Executor import Executor


class ReadDatasetFormatParamsExecutor(Executor):
    def __init__(self) -> None:
        self.inputShape = None
        self.numberOfClasses = None
        super().__init__()

    def execute(self, datasetPathWithoutFormat, chosenFormat):
        super().execute()

    def getInputShape(self):
        return self.inputShape

    def getNumberOfClasses(self):
        return self.numberOfClasses
