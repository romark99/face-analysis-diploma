from domain.model.executor.Executor import Executor


class ChooseDatasetFormatNameExecutor(Executor):

    def __init__(self):
        self.datasetFormatName = None

    def getDatasetFormatName(self):
        return self.datasetFormatName
