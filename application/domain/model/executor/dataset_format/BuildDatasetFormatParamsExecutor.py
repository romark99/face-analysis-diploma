from domain.model.executor.Executor import Executor


class BuildDatasetFormatParamsExecutor(Executor):
    def __init__(self) -> None:
        self.inputShape = None
        self.numberOfClasses = None
        super().__init__()

    def execute(self):
        super().execute()

    def getInputShape(self):
        return self.inputShape

    def getNumberOfClasses(self):
        return self.numberOfClasses
