from domain.model.executor.Executor import Executor


class SaveDatasetFormatParamsExecutor(Executor):
    def execute(self, datasetPathWithoutFormat, datasetFormat):
        super().execute()
