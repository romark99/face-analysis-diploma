from domain.model.executor.Executor import Executor


class GetDatasetFormatsFromDatasetFolderExecutor(Executor):
    def __init__(self):
        self.datasetFormats = None

    def execute(self, datasetPath):
        super().execute()

    def getDatasetFormats(self):
        return self.datasetFormats
