from domain.model.executor.Executor import Executor


class GetDataReaderByDatasetFormatExecutor(Executor):

    def __init__(self) -> None:
        self.dataReader = None
        super().__init__()

    def execute(self, datasetFormat):
        super().execute(self)

    def getDataReader(self):
        return self.dataReader
