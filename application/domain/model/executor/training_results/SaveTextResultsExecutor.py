from domain.model.executor.Executor import Executor


class SaveTextResultsExecutor(Executor):
    def execute(self, kerasHistory, resultsFolderPath, totalTime):
        super().execute()
