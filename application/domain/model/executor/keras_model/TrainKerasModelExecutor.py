from domain.model.executor.Executor import Executor


class TrainKerasModelExecutor(Executor):

    def __init__(self) -> None:
        self.kerasHistory = None
        self.totalTime = None
        super().__init__()

    def execute(self, kerasModel, netData, netParams, saveCallbacks):
        super().execute(self)

    def getKerasHistory(self):
        return self.kerasHistory

    def getTotalTime(self):
        return self.totalTime
