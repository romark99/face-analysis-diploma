from domain.model.executor.Executor import Executor


class ReadYesNoExecutor(Executor):

    def __init__(self) -> None:
        self.result = None

    def getResult(self):
        return self.result
