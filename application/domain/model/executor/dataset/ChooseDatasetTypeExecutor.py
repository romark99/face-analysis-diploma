from domain.model.executor.Executor import Executor


class ChooseDatasetTypeExecutor(Executor):

    def __init__(self):
        self.datasetType = None

    def getDatasetType(self):
        return self.datasetType
