from domain.model.executor.Executor import Executor


class ChooseDataFolderExecutor(Executor):

    def __init__(self):
        self.datasetFolderPath = None

    def getDatasetFolderPath(self):
        return self.datasetFolderPath
