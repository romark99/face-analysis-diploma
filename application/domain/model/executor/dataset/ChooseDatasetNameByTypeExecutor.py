from domain.model.executor.Executor import Executor


class ChooseDatasetNameByTypeExecutor(Executor):

    def __init__(self) -> None:
        self.datasetName = None
        super().__init__()

    def execute(self, datasetType):
        super().execute()

    def getDatasetName(self):
        return self.datasetName
