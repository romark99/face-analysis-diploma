from domain.model.dataset.Dataset import Dataset
from domain.model.runner.Runner import Runner


class ChooseDatasetRunner(Runner):
    def __init__(self, chooseDatasetTypeExecutor, chooseDatasetNameByTypeExecutor,
                 determineDatasetFormatRunner) -> None:
        self.dataset = None
        self.chooseDatasetTypeExecutor = chooseDatasetTypeExecutor
        self.chooseDatasetNameByTypeExecutor = chooseDatasetNameByTypeExecutor
        self.determineDatasetFormatRunner = determineDatasetFormatRunner
        self.datasetType = None
        super().__init__()

    def run(self):
        dataset = Dataset()

        if self.datasetType is None:
            self.chooseDatasetTypeExecutor.execute()
            self.datasetType = self.chooseDatasetTypeExecutor.getDatasetType()

        dataset.type = self.datasetType

        self.chooseDatasetNameByTypeExecutor.execute(dataset.type)
        dataset.name = self.chooseDatasetNameByTypeExecutor.getDatasetName()
        if dataset.name is None:
            self.dataset = None
            return

        self.determineDatasetFormatRunner.setDataset(dataset)
        self.determineDatasetFormatRunner.run()
        dataset = self.determineDatasetFormatRunner.getDataset()

        if dataset.format is None:
            self.dataset = None
            return

        dataset.determinePath()
        self.dataset = dataset

    def getDataset(self):
        return self.dataset

    def setDatasetType(self, datasetType):
        self.datasetType = datasetType
