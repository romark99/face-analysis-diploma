from domain.model.runner.Runner import Runner


class DetermineDatasetRunner(Runner):
    def __init__(self, readYesNoExecutor, buildDatasetRunner,
                 chooseDatasetRunner) -> None:
        self.readYesNoExecutor = readYesNoExecutor
        self.buildDatasetRunner = buildDatasetRunner
        self.chooseDatasetRunner = chooseDatasetRunner
        self.dataset = None
        self.datasetType = None
        super().__init__()

    def run(self):
        self.readYesNoExecutor.execute()
        isChooseDataset = self.readYesNoExecutor.getResult()
        if isChooseDataset:
            self.chooseDatasetRunner.setDatasetType(self.datasetType)
            self.chooseDatasetRunner.run()
            self.dataset = self.chooseDatasetRunner.getDataset()
        else:
            self.buildDatasetRunner.setDatasetType(self.datasetType)
            self.buildDatasetRunner.run()
            self.dataset = self.buildDatasetRunner.getDataset()

    def getDataset(self):
        return self.dataset

    def setDatasetType(self, datasetType):
        self.datasetType = datasetType
