from domain.model.runner.Runner import Runner


class SaveDatasetRunner(Runner):
    def __init__(self, copyDatasetExecutor, saveDatasetFormatParamsExeuctor) -> None:
        self.dataset = None
        self.fromFolderPath = None
        self.copyDatasetExecutor = copyDatasetExecutor
        self.saveDatasetFormatParamsExecutor = saveDatasetFormatParamsExeuctor
        super().__init__()

    def run(self) -> None:
        self.copyDatasetExecutor.execute(self.dataset, self.fromFolderPath)

        datasetPathWithoutFormat = self.dataset.getPathWithoutFormat()
        datasetFormat = self.dataset.format
        self.saveDatasetFormatParamsExecutor.execute(datasetPathWithoutFormat, datasetFormat)

    def setDataset(self, dataset):
        self.dataset = dataset

    def setFromFolderPath(self, fromFolderPath):
        self.fromFolderPath = fromFolderPath
