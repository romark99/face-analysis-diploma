import os

from domain.model.dataset.Dataset import Dataset
from domain.model.runner.Runner import Runner


class BuildDatasetRunner(Runner):
    def __init__(self, chooseDatasetTypeExecutor, chooseDataFolderExecutor, buildDatasetFormatRunner,
                 saveDatasetRunner) -> None:
        self.dataset = None
        self.chooseDatasetTypeExecutor = chooseDatasetTypeExecutor
        self.chooseDataFolderExecutor = chooseDataFolderExecutor
        self.buildDatasetFormatRunner = buildDatasetFormatRunner
        self.saveDatasetRunner = saveDatasetRunner
        self.datasetType = None
        super().__init__()

    def run(self):
        dataset = Dataset()

        if self.datasetType is None:
            self.chooseDatasetTypeExecutor.execute()
            self.datasetType = self.chooseDatasetTypeExecutor.getDatasetType()

        dataset.type = self.datasetType

        self.chooseDataFolderExecutor.execute()
        fromPath = self.chooseDataFolderExecutor.getDatasetFolderPath()

        if fromPath is None:
            return

        dataset.name = os.path.basename(fromPath)

        self.buildDatasetFormatRunner.run()

        dataset.format = self.buildDatasetFormatRunner.getDatasetFormat()

        dataset.determinePath()

        self.saveDatasetRunner.setDataset(dataset)
        self.saveDatasetRunner.setFromFolderPath(fromPath)
        self.saveDatasetRunner.run()

        self.dataset = dataset

    def getDataset(self):
        return self.dataset

    def setDatasetType(self, datasetType):
        self.datasetType = datasetType
