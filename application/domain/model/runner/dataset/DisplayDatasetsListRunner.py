from domain.model.runner.Runner import Runner


class DisplayDatasetsListRunner(Runner):
    def __init__(self, displayDatasetsListExecutor) -> None:
        self.displayDatasetsListExecutor = displayDatasetsListExecutor
        super().__init__()

    def run(self):
        self.displayDatasetsListExecutor.execute()
