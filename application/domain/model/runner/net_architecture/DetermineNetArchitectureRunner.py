from domain.model.runner.Runner import Runner


class DetermineNetArchitectureRunner(Runner):
    def __init__(self, netArchitectureRepo) -> None:
        self.netArchitectureRepo = netArchitectureRepo
        self.netArchitecture = None
        self.datasetType = None
        super().__init__()

    def run(self):
        # TODO: maybe should be implemented
        super().run()

    def getNetArchitecture(self):
        return self.netArchitecture

    def setDatasetType(self, datasetType):
        self.datasetType = datasetType
