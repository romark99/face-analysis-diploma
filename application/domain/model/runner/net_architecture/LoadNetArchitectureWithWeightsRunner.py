from domain.model.net_architecture.NetArchitecture import NetArchitecture
from domain.model.runner.Runner import Runner


class LoadNetArchitectureWithWeightsRunner(Runner):
    def __init__(self, netWeightsReader, netWeightsRepo, chooseNetArchitectureRunner,
                 netArchitectureRepository) -> None:
        self.netWeightsReader = netWeightsReader
        self.netWeightsRepo = netWeightsRepo
        self.chooseNetArchitectureRunner = chooseNetArchitectureRunner
        self.netArchitectureRepository = netArchitectureRepository
        self.netArchitecture = None
        self.netWeights = None
        self.datasetType = None
        super().__init__()

    def readWeightPath(self):
        raise NotImplementedError("'readWeightPath()' method should be overridden.")

    def chooseModel(self, models):
        raise NotImplementedError("'chooseModel()' method should be overridden.")

    def loadWeightsToNetArchitecture(self, netArchitecture, name, weightModel):
        raise NotImplementedError("'loadWeightsToNetArchitecture()' method should be overridden.")

    def run(self):
        self.netWeights = self.netWeightsReader.read(self.datasetType)
        if self.netWeights is None:
            self.netArchitecture = None
            return

        weightModel = self.chooseModel(self.netWeights.models)
        if weightModel is None:
            self.netArchitecture = None
            return

        netArchitectureName = self.netWeights.netArchitectureName

        if netArchitectureName is None:
            self.chooseNetArchitectureRunner.run(self.datasetType)
            netArchitecture = self.chooseNetArchitectureRunner.getNetArchitecture()
        else:
            specification = NetArchitecture(type=self.datasetType, name=netArchitectureName)
            netArchitecture = self.netArchitectureRepository.getEntity(specification)

        if netArchitecture is None:
            self.netArchitecture = None
            return

        netArchitecture = self.loadWeightsToNetArchitecture(netArchitecture, self.netWeights.name, weightModel)
        self.netArchitecture = netArchitecture

    def getNetArchitecture(self):
        return self.netArchitecture

    def getNetWeights(self):
        return self.netWeights

    def setDatasetType(self, datasetType):
        self.datasetType = datasetType
