from domain.model.net_architecture.NetArchitecture import NetArchitecture
from domain.model.runner.Runner import Runner


class ChooseNetArchitectureRunner(Runner):
    def __init__(self, archRepo) -> None:
        self.archRepo = archRepo
        self.netArchitecture = None

    def readNetArchitectureName(self, architectures):
        raise NotImplementedError("'readNetArchitectureName()' method should be overridden.")

    def getNetArchitecture(self):
        return self.netArchitecture

    def run(self, datasetType) -> None:
        specification = NetArchitecture(type=datasetType)
        architectures = self.archRepo.getEntities(specification)
        architectureNames = [arch.name for arch in architectures]

        netArchitectureName = self.readNetArchitectureName(architectureNames)
        if netArchitectureName is None:
            return None

        specification.name = netArchitectureName
        self.netArchitecture = self.archRepo.getEntity(specification)
