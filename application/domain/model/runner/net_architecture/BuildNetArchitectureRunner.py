from domain.model.runner.Runner import Runner


class BuildNetArchitectureRunner(Runner):
    def __init__(self, netArchitectureReader,
                 netArchitectureRepository) -> None:
        self.netArchitectureReader = netArchitectureReader
        self.netArchitectureRepository = netArchitectureRepository
        self.netArchitecture = None
        super().__init__()

    def run(self, datasetType=None):
        netArchitecture = self.netArchitectureReader.read(datasetType)

        addedEntity = self.netArchitectureRepository.add(netArchitecture)
        self.netArchitecture = addedEntity

    def getNetArchitecture(self):
        return self.netArchitecture
