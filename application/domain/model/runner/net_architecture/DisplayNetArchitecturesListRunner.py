from domain.model.runner.Runner import Runner


class DisplayNetArchitecturesListRunner(Runner):
    def __init__(self, netArchitectureRepo):
        self.netArchitectureRepo = netArchitectureRepo

    def run(self):
        architectures = self.readNetArchitectures()
        self.displayArchitectures(architectures)

    def readNetArchitectures(self):
        return self.netArchitectureRepo.getEntities()

    def displayArchitectures(self, architectures):
        raise NotImplementedError("'displayArchitectures()' method should be overridden.")
