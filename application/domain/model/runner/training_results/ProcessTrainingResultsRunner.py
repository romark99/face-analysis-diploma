from domain.model.runner.Runner import Runner


class ProcessTrainingResultsRunner(Runner):
    def __init__(self, savePlotsExecutor,
                 saveErrorMatrixExecutor,
                 saveTextResultsExecutor,
                 displayTextResultsExecutor):
        self.kerasHistory = None
        self.resultsFolderPath = None
        self.totalTime = None
        self.imageNames = None
        self.savePlotsExecutor = savePlotsExecutor
        self.saveErrorMatrixExecutor = saveErrorMatrixExecutor
        self.saveTextResultsExecutor = saveTextResultsExecutor
        self.displayTextResultsExecutor = displayTextResultsExecutor
        super().__init__()

    def run(self) -> None:
        self.savePlotsExecutor.execute(self.kerasHistory, self.resultsFolderPath)
        self.saveErrorMatrixExecutor.execute(self.kerasHistory, self.resultsFolderPath)

        self.saveTextResultsExecutor.execute(self.kerasHistory, self.resultsFolderPath, self.totalTime)

        self.displayTextResultsExecutor.execute(self.resultsFolderPath)

    def setKerasHistory(self, kerasHistory):
        self.kerasHistory = kerasHistory

    def setResultsFolderPath(self, resultsFolderPath):
        self.resultsFolderPath = resultsFolderPath

    def setTotalTime(self, totalTime):
        self.totalTime = totalTime

    def setImageNames(self, imageNames):
        self.imageNames = imageNames
