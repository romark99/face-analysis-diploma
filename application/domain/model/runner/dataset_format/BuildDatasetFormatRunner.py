from domain.model.dataset_format.DatasetFormat import DatasetFormat
from domain.model.runner.Runner import Runner


class BuildDatasetFormatRunner(Runner):
    def __init__(self, chooseDatasetFormatNameExecutor,
                 buildDatasetFormatParamsExecutor) -> None:
        self.datasetFormat = None
        self.chooseDatasetFormatNameExecutor = chooseDatasetFormatNameExecutor
        self.buildDatasetFormatParamsExecutor = buildDatasetFormatParamsExecutor

    def run(self):
        datasetFormat = DatasetFormat()

        self.chooseDatasetFormatNameExecutor.execute()
        datasetFormat.name = self.chooseDatasetFormatNameExecutor.getDatasetFormatName()
        datasetFormat.determineDescription()

        self.buildDatasetFormatParamsExecutor.execute()
        datasetFormat.inputShape = self.buildDatasetFormatParamsExecutor.getInputShape()
        datasetFormat.numberOfClasses = self.buildDatasetFormatParamsExecutor.getNumberOfClasses()

        self.datasetFormat = datasetFormat

    def getDatasetFormat(self):
        return self.datasetFormat
