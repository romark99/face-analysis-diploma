from domain.model.runner.Runner import Runner


class ReformatDatasetFormatRunner(Runner):
    def __init__(self):
        self.datasetPathWithoutFormat = None
        self.inputShape = None
        self.newFormatName = None

    def setDatasetPathWithoutFormat(self, datasetPathWithoutFormat):
        self.datasetPathWithoutFormat = datasetPathWithoutFormat

    def setNewFormatName(self, newFormatName):
        self.newFormatName = newFormatName

    def setInputShape(self, inputShape):
        self.inputShape = inputShape

    def getNewFormatName(self):
        return self.newFormatName
