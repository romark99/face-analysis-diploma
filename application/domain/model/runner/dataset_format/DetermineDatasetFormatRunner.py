import os

from definitions import FILE_DATASET_PARAMS_INFO
from domain.model.dataset_format.DatasetFormat import DatasetFormat
from domain.model.runner.Runner import Runner


class DetermineDatasetFormatRunner(Runner):

    def __init__(self, chooseDatasetFormatNameExecutor, getDatasetFormatsFromDatasetFolderExecutor,
                 isToReformatYesNoExecutor, buildDatasetFormatParamsExecutor, reformatDatasetFormatRunner,
                 readDatasetFormatParamsExecutor, saveDatasetFormatParamsExecutor) -> None:
        self.dataset = None
        self.chooseDatasetFormatNameExecutor = chooseDatasetFormatNameExecutor
        self.getDatasetFormatsFromDatasetFolderExecutor = getDatasetFormatsFromDatasetFolderExecutor
        self.isToReformatYesNoExecutor = isToReformatYesNoExecutor
        self.buildDatasetFormatParamsExecutor = buildDatasetFormatParamsExecutor
        self.reformatDatasetFormatRunner = reformatDatasetFormatRunner
        self.readDatasetFormatParamsExecutor = readDatasetFormatParamsExecutor
        self.saveDatasetFormatParamsExecutor = saveDatasetFormatParamsExecutor
        super().__init__()

    def run(self, datasetFormatName=None):
        datasetFormat = DatasetFormat()

        if datasetFormatName is None:
            self.chooseDatasetFormatNameExecutor.execute()
            chosenFormatName = self.chooseDatasetFormatNameExecutor.getDatasetFormatName()
        else:
            chosenFormatName = datasetFormatName

        datasetPathWithoutFormat = self.dataset.getPathWithoutFormat()
        self.getDatasetFormatsFromDatasetFolderExecutor.execute(datasetPathWithoutFormat)

        datasetFormats = self.getDatasetFormatsFromDatasetFolderExecutor.getDatasetFormats()
        isExist = chosenFormatName in datasetFormats

        if not isExist:
            self.isToReformatYesNoExecutor.execute()
            isToReformat = self.isToReformatYesNoExecutor.getResult()

            if isToReformat:
                newFormatName = chosenFormatName

                self.buildDatasetFormatParamsExecutor.execute()
                datasetFormat.inputShape = self.buildDatasetFormatParamsExecutor.getInputShape()
                datasetFormat.numberOfClasses = self.buildDatasetFormatParamsExecutor.getNumberOfClasses()

                self.reformatDatasetFormatRunner.setDatasetPathWithoutFormat(datasetPathWithoutFormat)
                self.reformatDatasetFormatRunner.setInputShape(datasetFormat.inputShape)
                self.reformatDatasetFormatRunner.setNewFormatName(newFormatName)
                self.reformatDatasetFormatRunner.run()

                datasetFormat.name = self.reformatDatasetFormatRunner.getNewFormatName()
                if datasetFormat is None or datasetFormat.name is None:
                    return

                self.saveDatasetFormatParamsExecutor.execute(datasetPathWithoutFormat, datasetFormat)
            else:
                self.dataset.format = None
                print('Dataset format is not found.')
                return
        else:
            paramFilePath = os.path.join(datasetPathWithoutFormat, chosenFormatName, FILE_DATASET_PARAMS_INFO)
            if not os.path.exists(paramFilePath):
                self.buildDatasetFormatParamsExecutor.execute()
                datasetFormat.inputShape = self.buildDatasetFormatParamsExecutor.getInputShape()
                datasetFormat.numberOfClasses = self.buildDatasetFormatParamsExecutor.getNumberOfClasses()

                datasetFormat.name = chosenFormatName
                self.saveDatasetFormatParamsExecutor.execute(datasetPathWithoutFormat, datasetFormat)
            else:
                self.readDatasetFormatParamsExecutor.execute(datasetPathWithoutFormat, chosenFormatName)
                datasetFormat.inputShape = self.readDatasetFormatParamsExecutor.getInputShape()
                datasetFormat.numberOfClasses = self.readDatasetFormatParamsExecutor.getNumberOfClasses()

            if datasetFormat.inputShape is not None and datasetFormat.numberOfClasses is not None:
                datasetFormat.name = chosenFormatName

        self.dataset.format = datasetFormat
        self.dataset.determinePath()

    def setDataset(self, dataset):
        self.dataset = dataset

    def getDataset(self):
        return self.dataset
