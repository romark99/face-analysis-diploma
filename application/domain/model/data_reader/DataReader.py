class DataReader:
    def __init__(self) -> None:
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None

    def getData(self):
        return (self.x_train, self.x_test, self.y_train, self.y_test)

    def read(self, datasetFormatPath, inputShape, numberOfLabels):
        raise NotImplementedError("'read()' method should be overridden.")

    def readFromWildImagePath(self, imagePath, inputShape):
        raise NotImplementedError("'readFromWildImagePath()' method should be overridden.")

    def readFromCroppedNotScaledImageArray(self, croppedNotScaledFrame, inputShape):
        raise NotImplementedError("'readFromCroppedNotScaledImageArray()' method should be overridden.")
